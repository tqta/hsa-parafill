#ifndef CPUParaFill_h
#define CPUParaFill_h

#include "cConfiguration.h"
#include <vector>
#include "CImg.h"

void CPUParaFill(cConfiguration &Configuration, const std::vector<uint32_t> &MasterImage,
    const CImg<unsigned char> &InputImage);

#endif
