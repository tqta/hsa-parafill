#ifndef cExecutionContext_h
#define cExecutionContext_h

#include <sstream>

#include "cSpanner.h"
#include "cWorkToken.h"

#include "UtilityFunctions.h"

typedef struct sThreadDebugInfo
{
  size_t CycleCount;
  size_t StarvedCycleCount;
  size_t SpannerCount;
} tThreadDebugInfo;


#define DumpField(Field) #Field "=" << Context.Field
inline std::ostream &operator << (std::ostream &Stream, const tThreadDebugInfo &Context)
{
  Stream << DumpField(CycleCount) << ", " DumpField(StarvedCycleCount) << ", " << DumpField(SpannerCount);

  return Stream;
}
#undef DumpField

typedef struct sGPUExecutionContext
{
  uint32_t *ThreadTempExpected;
  uint32_t *Image;
  tWorkToken *WorkQueue;
  tSpanner LeftSpanner;
  tSpanner RightSpanner;
  tSpanner UpSpanner;
  tSpanner DownSpanner;
  int Width;
  int Height;
  int nPixels;
  uint32_t WorkgroupSize;
  uint32_t nWorkgroups;
  uint32_t nThreads;

  // Colors are stored 0xxxRRGGBB
  uint32_t InteriorColor;
  uint32_t FillColor;

  uint32_t nThreadsWorking;

  uint32_t nQueued;
#ifdef __Debug__
  uint32_t nEnqueued;
  uint32_t nDequeued;
#endif
  uint32_t QueueSize;
  uint32_t WorkQueueFront;
  uint32_t WorkQueueRear;
  uint32_t WorkQueueTempRear;
#ifdef __Debug__
  // Debugging stuff goes here.
  uint32_t CyclesDone;
  tThreadDebugInfo *ThreadDebugInfo;
#endif
  double Buffer[0];
} tGPUExecutionContext;

#define DumpField(Field) #Field "=" << Context.Field
#define DumpHexField(Field) #Field "=" << Hex(Context.Field)
#define DumpAddrField(Field) #Field "=" << Addr(Context.Field)
#define DumpStructField(Field) #Field "(" << Addr(&Context.Field) << ") [" << Context.Field << "]"
inline std::ostream &operator << (std::ostream &Stream, const tGPUExecutionContext &Context)
{
  Stream << DumpField(Width) << std::endl;
  Stream << DumpField(Height) << std::endl;
  Stream << DumpField(nPixels) << std::endl;
  Stream << DumpHexField(InteriorColor) << std::endl;
  Stream << DumpHexField(FillColor) << std::endl;
  Stream << DumpField(WorkgroupSize) << std::endl;
  Stream << DumpField(nWorkgroups) << std::endl;
  Stream << DumpField(nThreadsWorking) << std::endl;
  Stream << DumpField(nThreads) << std::endl;
  Stream << DumpStructField(LeftSpanner) << std::endl;
  Stream << DumpStructField(RightSpanner) << std::endl;
  Stream << DumpStructField(UpSpanner) << std::endl;
  Stream << DumpStructField(DownSpanner) << std::endl;
  Stream << DumpAddrField(Image) << std::endl;
  Stream << DumpField(nQueued) << std::endl;
#ifdef __Debug__
  Stream << DumpField(nEnqueued) << std::endl;
  Stream << DumpField(nDequeued) << std::endl;
#endif
  Stream << DumpField(WorkQueueFront) << std::endl;
  Stream << DumpField(WorkQueueRear) << std::endl;
  Stream << DumpField(WorkQueueTempRear) << std::endl;
  Stream << DumpAddrField(WorkQueue) << std::endl;
  for (auto i = Context.WorkQueueFront; i < Context.WorkQueueRear; ++i)
  {
    Stream << "  Q[" << i << "]: " << Context.WorkQueue[i] << std::endl;
  }

#ifdef __Debug__
  Stream << "************************************Debug Info************************************" << std::endl;
  for (auto i = 0u; i < Context.nThreads; ++i)
  {
    Stream << "  ThreadDebugInfo[" << i << "]: " << Context.ThreadDebugInfo[i] << std::endl;
  }
  Stream << DumpField(CyclesDone) << std::endl;
#endif
  return Stream;
}
#undef DumpField
#undef DumpHexField
#undef DumpAddrField
#undef DumpStructField

#endif
