#ifndef cConfiguration_h
#define cConfiguration_h

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>

#include "cPoint.h"
#include "cRGBColor.h"

struct cConfiguration
{
  inline std::ostream &StatsOut(void) {return Stats ? *((std::ostream *)Stats) : std::cout;}

  std::string ID(const std::string Sep = ", ") const;
  void ParseCommandLine(int argc, char *argv[]);

  unsigned int Platform;
  unsigned int Device;
  unsigned int WorkgroupSize;
  unsigned int nWorkgroups;
  unsigned int nThreads;
  unsigned int nIterations;
  std::vector<cPoint> StartingPoints;
  cRGBColor FillColor;
  bool OpenCLVerbose;
  bool OpenCLUseAtomics;
  bool OpenCLUseFineGrainBuffer;
  const char  *KernelFilename;
  const char  *ImageFilename;
  const char  *CPUOutputFilename;
  const char  *GPUOutputFilename;
  const char  *StatsFilename;
  const char  *Core;
  std::ofstream *Stats;
};

typedef struct cConfiguration tConfiguration;

#endif
