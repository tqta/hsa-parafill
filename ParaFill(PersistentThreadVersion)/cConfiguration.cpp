#include "cConfiguration.h"

#include <sstream>

std::string
cConfiguration::ID(const std::string Sep) const
{
  std::stringstream IDStream;

#define DumpConfigurationField(Field) #Field "=" << Field
  IDStream
  << DumpConfigurationField(Platform)
  << Sep << DumpConfigurationField(Device)
  << Sep << DumpConfigurationField(WorkgroupSize)
  << Sep << DumpConfigurationField(nWorkgroups)
  << Sep << DumpConfigurationField(nIterations)
  << Sep << DumpConfigurationField(KernelFilename)
  << Sep << DumpConfigurationField(ImageFilename)
  << Sep << DumpConfigurationField(CPUOutputFilename)
  << Sep << DumpConfigurationField(GPUOutputFilename)
  << Sep << DumpConfigurationField(StatsFilename)
//  << Sep << DumpConfigurationField(StartingPoint)
  << Sep << DumpConfigurationField(FillColor)
  << Sep << DumpConfigurationField(OpenCLVerbose)
  << Sep << DumpConfigurationField(OpenCLUseAtomics)
  << Sep << DumpConfigurationField(OpenCLUseFineGrainBuffer)
  << std::endl;
#undef DumpConfigurationField

  return IDStream.str();
}

void
cConfiguration::ParseCommandLine(int argc, char *argv[])
{
  bool Failed = false;

  // Parse comand line for configuration.
  for (int arg = 1; arg < argc; ++arg)
  {
    std::string ThisArg(argv[arg]);
    if (ThisArg == "-p")
    {
      if (++arg < argc)
      {
        Platform = std::stoi(argv[arg], NULL, 0);
      }
      else
      {
        Failed = true;
      }
    }
    else if (ThisArg == "-k")
    {
      if (++arg < argc)
      {
        KernelFilename = argv[arg];
      }
    }
    else if (ThisArg == "-i")
    {
      if (++arg < argc)
      {
        ImageFilename = argv[arg];
      }
    }
    else if (ThisArg == "-co")
    {
      if (++arg < argc)
      {
        CPUOutputFilename = argv[arg];
      }
    }
    else if (ThisArg == "-go")
    {
      if (++arg < argc)
      {
        GPUOutputFilename = argv[arg];
      }
    }
    else if (ThisArg == "-stats")
    {
      if (++arg < argc)
      {
        StatsFilename = argv[arg];
      }
    }
    else if (ThisArg == "-core")
    {
      if (++arg < argc)
      {
        Core = argv[arg];
      }
    }
    else if (ThisArg == "-s")
    {
      cPoint StartingPoint;
      if (++arg < argc)
      {
        StartingPoint.x = std::stoi(argv[arg], NULL, 0);
        if (++arg < argc)
        {
          StartingPoint.y = std::stoi(argv[arg], NULL, 0);
          StartingPoints.push_back(StartingPoint);
        }
        else
        {
          Failed = true;
        }
      }
      else
      {
        Failed = true;
      }
    }
    else if (ThisArg == "-f")
    {
      FillColor.z = 0;
      if (++arg < argc)
      {
        FillColor.r = std::stoi(argv[arg], NULL, 0);
        if (++arg < argc)
        {
          FillColor.g = std::stoi(argv[arg], NULL, 0);
          if (++arg < argc)
          {
            FillColor.b = std::stoi(argv[arg], NULL, 0);
          }
          else
          {
            Failed = true;
          }
        }
        else
        {
          Failed = true;
        }
      }
      else
      {
        Failed = true;
      }
    }
    else if (ThisArg == "-nWG")
    {
      if (++arg < argc)
      {
        nWorkgroups = std::stoi(argv[arg], NULL, 0);
      }
      else
      {
        Failed = true;
      }
    }
    else if (ThisArg == "-WGSize")
    {
      if (++arg < argc)
      {
        WorkgroupSize = std::stoi(argv[arg], NULL, 0);
      }
      else
      {
        Failed = true;
      }
    }
    else if (ThisArg == "-i")
    {
      if (++arg < argc)
      {
        nIterations = std::stoi(argv[arg], NULL, 0);
      }
      else
      {
        Failed = true;
      }
    }
    else if (ThisArg == "-t")
    {
      if (++arg < argc)
      {
        nThreads = std::stoi(argv[arg], NULL, 0);
      }
      else
      {
        Failed = true;
      }
    }
    else
    {
      Failed = true;
    }

    if (Failed)
    {
      std::cerr << "Usage: <prog> -p <Platform> -d <Device> -k <KernelFilename> -stats <StateFilename>"
          "-i <GPUImageFilename> -co <CPUOutputFilename> -go <GPUOutputFilename> -core CPU|GPU "
          "-s <StartingX> <StartingY> [-s <StartingX> <StartingY> ...] -f <Red> <Blue> <Green> "
          "-nWG <nWorkgroups> -WGSize <WorkgroupSize> -i <Iterations> -t <nThreads>" << std::endl;
      exit(1);
    }
  }

  return;
}
