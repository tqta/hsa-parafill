#ifndef GPUHelper_h
#define GPUHelper_h

#include <string>
#include <vector>
#include <CL/cl.h>

struct sOpenCLConfiguration
{
  // No default constructor.
  sOpenCLConfiguration(void) = delete;

  sOpenCLConfiguration
  (
      const std::string  _KernelFilename                       ,
            cl_uint      _PlatformIndex                   = 0u  ,
            cl_uint      _DeviceIndex                     = 0u  ,
            bool         _Verbose                         = true,
            bool         _UseAtomics                      = true,
            bool         _UseFineGrainedBuffers           = true,
            cl_queue_properties _CommandQueueProperties   = 0,
      const std::string &_CompileFlags                    = "-cl-std=CL2.0"
  );

  ~sOpenCLConfiguration(void);

  // Handy implicit cast operators for OpenCL objects.
  inline operator cl_platform_id  () const { return PlatformID;   };
  inline operator cl_program      () const { return Program;      };
  inline operator cl_device_id    () const { return DeviceID;     };
  inline operator cl_context      () const { return Context;      };
  inline operator cl_command_queue() const { return CommandQueue; };

  cl_kernel CreateKernel(const std::string &KernelFunctionName);

  template<typename Type>
  Type GetDeviceInfo(cl_device_info ParamName)
  {
    cl_int rc = CL_SUCCESS;

    Type Parm;

    rc = clGetDeviceInfo(DeviceID, ParamName, sizeof(Parm), &Parm, NULL);
    HandleOpenCLError(rc, "clGetDeviceInfo", __FILE__, __LINE__);

    return Parm;
  }

  template<typename Type>
  Type *SVMAllocate(size_t N = 1u, const cl_svm_mem_flags &SVMFlags = CL_MEM_READ_WRITE, const unsigned int &Alignment = sizeof(double))
  {
    cl_svm_mem_flags Flags = SVMFlags;
    if (UseFineGrainedBuffers)
    {
      Flags |= CL_MEM_SVM_FINE_GRAIN_BUFFER;
    }
    else
    {
      Flags &= ~CL_MEM_SVM_FINE_GRAIN_BUFFER;
    }

    if (UseAtomics)
    {
      Flags |= CL_MEM_SVM_ATOMICS;
    }
    else
    {
      Flags &= ~CL_MEM_SVM_ATOMICS;
    }

    Type *Object = (Type *) clSVMAlloc(Context, Flags, sizeof(Type) * N, Alignment);

    return Object;
  }

  static std::string GetDeviceInfoString(cl_device_id &DeviceID, cl_device_info ParamName);
  inline std::string GetDeviceInfoString(cl_device_info ParamName)
  {
    return GetDeviceInfoString(DeviceID, ParamName);
  };

  template<typename Type>
  static Type GetKernelWorkGroupInfo(cl_kernel Kernel, cl_device_id Device, cl_kernel_work_group_info ParamName)
  {
    Type Temp;
    cl_int rc = CL_SUCCESS;
    rc = clGetKernelWorkGroupInfo(Kernel, Device, ParamName, sizeof(Type), &Temp, nullptr);
    HandleOpenCLError(rc, "clGetKernelWorkGroupInfo", __FILE__, __LINE__);

    return Temp;
  };

  static std::string GetPlatformInfoString(cl_platform_id &PlatformID, cl_platform_info ParamName);
  inline std::string GetPlatformInfoString(cl_platform_info ParamName)
  {
    return GetPlatformInfoString(PlatformID, ParamName);
  };

  std::string GetProgramInfoString(cl_program_info ParamName) const;
  static void HandleOpenCLError(cl_int rc, const char *Function, const char *File, int Line);

  std::string                 KernelFilename;
  cl_uint                     PlatformIndex;
  cl_uint                     DeviceIndex;
  bool                        Verbose;
  bool                        UseAtomics;
  bool                        UseFineGrainedBuffers;

  // OpenCL variables.
  cl_platform_id              PlatformID;
  cl_program                  Program;
  cl_device_id                DeviceID;
  cl_context                  Context;
  cl_command_queue            CommandQueue;

  cl_uint                     nPlatforms;
  cl_uint                     nDevices;
  std::vector<cl_platform_id> PlatformIDs;
  std::vector<cl_device_id>   DeviceIDs;
};

#endif
