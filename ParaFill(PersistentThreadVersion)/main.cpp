#include <algorithm>
#include <assert.h>
#include <iostream>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>

#include "APUHelper.h"
#include "cConfiguration.h"
#include "CPUParaFill.h"
#include "GPUParaFill.h"

void Initialize(cConfiguration &Parms, int argc, char *argv[]);

// Images
CImg<unsigned char> InputImage;

int main(int argc, char *argv[])
{
  try
  {
    // The following code depends on this alignment so that CPU and GPU
    // data structure aligns correctly.
    assert((sizeof(tWorkToken) % 8u) == 0);

    cConfiguration Configuration;
    Initialize(Configuration, argc, argv);

#ifdef __Debug__
    std::cout << Configuration.ID() << std::endl;
#endif

    // Read the image.
    InputImage.load(Configuration.ImageFilename);
    int Width = InputImage.width();
    int Height = InputImage.height();
    int nPixels = Width * Height;

    // Create master image.
    cRGBColor PixelColor;
    std::vector<uint32_t> MasterImage;
    MasterImage.reserve(nPixels);

    for (auto y = 0; y < Height; ++y)
    {
      for (auto x = 0; x < Width; ++x)
      {
        PixelColor.GetPixelColor(InputImage, x, y);
        MasterImage.push_back(PixelColor.ToUInt());
      }
    }

    Configuration.StatsOut() << "[Stats]" << std::endl;
    Configuration.StatsOut() << "Iterations=" << Configuration.nIterations << std::endl;
    Configuration.StatsOut() << "CoreType=\"" << Configuration.Core << "\"" << std::endl;

    std::string TempCore = Configuration.Core;
    if (TempCore == "gpu")
    {
      // Open OpenCL 2.0
      sOpenCLConfiguration OpenCLConfiguration(Configuration.KernelFilename, Configuration.Platform, Configuration.Device,
          Configuration.OpenCLVerbose, Configuration.OpenCLUseAtomics, Configuration.OpenCLUseFineGrainBuffer,
          CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE | CL_QUEUE_PROFILING_ENABLE,
#ifdef __Debug__
          "-cl-std=CL2.0 -D__Debug__"
#else
          "-cl-std=CL2.0"
#endif
          );

      GPUParaFill(OpenCLConfiguration, Configuration, MasterImage, InputImage);
    } else if (TempCore == "cpu")
    {
      Configuration.StatsOut() << "nThreads=" << Configuration.nThreads << std::endl;

      CPUParaFill(Configuration, MasterImage, InputImage);
    }

    Configuration.Stats->close();
  }
  catch (const char *ErrorMessage)
  {
    std::cout << "Error caught: " << ErrorMessage << std::endl;
    return 1;
  }

#ifdef __Debug__
  std::cout << "****************************** End Execution ******************************" << std::endl;
#endif

  return 0;
}

void Initialize(cConfiguration &Parms, int argc, char *argv[])
{
  std::cout.imbue(std::locale(""));

  // Attempt max priority to mitigate OS thread scheduling effects.
  int ThreadPriority = -20;
#ifdef __Debug__
  int ThreadPriorityRC = setpriority(PRIO_PROCESS, getpid(), ThreadPriority);
  if (ThreadPriorityRC == 0)
  {
    std::cout << "Thread priority set to " << ThreadPriority << " OK." << std::endl;
  }
  else
  {
    std::cout << "Thread priority set to " << ThreadPriority << " failed: " << strerror(errno) << std::endl;
  }
#else
  setpriority(PRIO_PROCESS, getpid(), ThreadPriority);
#endif

  cConfiguration Configuration =
      {
        Platform                 : 0u,
        Device                   : 0u,
        WorkgroupSize            : 64u,
        nWorkgroups              : 7u,
        nThreads                 : 3u,
        nIterations              : 100u,
        StartingPoints           : {  },
        FillColor                : cRGBColor(0, 0, 255),

#ifdef __Debug__
        OpenCLVerbose            : true,
#else
        OpenCLVerbose            : false,
#endif
        OpenCLUseAtomics         : false,
        OpenCLUseFineGrainBuffer : true,
        KernelFilename           : "./Kernel.cl",
        ImageFilename            : "./Jigsaw.bmp",
        CPUOutputFilename        : "./CPUJigsawOut.png",
        GPUOutputFilename        : "./GPUJigsawOut.png",
        StatsFilename            : "./Stats.ini",
        Core                     : "gpu",
        Stats                    : nullptr,
      };

  Configuration.ParseCommandLine(argc, argv);

  if (Configuration.Core)
  {
    std::string Temp = Configuration.Core;
    std::transform(Temp.begin(), Temp.end(), Temp.begin(), ::tolower);
    if (Temp == "cpu")
    {
      Configuration.Core = "cpu";
    } else if (Temp == "gpu")
    {
      Configuration.Core = "gpu";
    }

    if (Configuration.StatsFilename)
    {
      Configuration.Stats = new std::ofstream(Configuration.StatsFilename);
    }
    else
    {
      Configuration.Stats = nullptr;
    }
  }
  else
  {
    Configuration.Core = "";
  }

  if (Configuration.StartingPoints.size() == 0u)
  {
    Configuration.StartingPoints.push_back(cPoint(100, 300));
  }
  // Transfer configuration to caller.
  Parms = Configuration;

  return;
}

