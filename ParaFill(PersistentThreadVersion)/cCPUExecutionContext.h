#ifndef cCPUExecutionContext_h
#define cCPUExecutionContext_h

#include <ostream>
#include <vector>

#include "cSpanner.h"
#include "cWorkToken.h"

#include "UtilityFunctions.h"

typedef struct sCPUExecutionContext
{
  std::vector<uint32_t> Image;
  cQueue<tWorkToken> WorkQueue;
  tSpanner LeftSpanner;
  tSpanner RightSpanner;
  tSpanner UpSpanner;
  tSpanner DownSpanner;
  int Width;
  int Height;
  int nPixels;

  // Colors are stored 0xxxRRGGBB
  uint32_t InteriorColor;
  uint32_t FillColor;

  // Thread info.
  uint32_t nThreadsToUse;
  std::atomic<uint32_t> nThreadsActive;

  // Debugging stuff goes here.
  std::vector<uint32_t> Cycles;
} tCPUExecutionContext;


#define DumpField(Field) #Field "=" << Context.Field
#define DumpHexField(Field) #Field "=" << Hex(Context.Field)
#define DumpAddrField(Field) #Field "=" << Addr(Context.Field)
#define DumpStructField(Field) #Field "(" << Addr(&Context.Field) << ") [" << Context.Field << "]"
inline std::ostream &operator << (std::ostream &Stream, const tCPUExecutionContext &Context)
{
  Stream << DumpField(Width) << std::endl;
  Stream << DumpField(Height) << std::endl;
  Stream << DumpField(nPixels) << std::endl;
  Stream << DumpHexField(InteriorColor) << std::endl;
  Stream << DumpHexField(FillColor) << std::endl;
  Stream << DumpStructField(LeftSpanner) << std::endl;
  Stream << DumpStructField(RightSpanner) << std::endl;
  Stream << DumpStructField(UpSpanner) << std::endl;
  Stream << DumpStructField(DownSpanner) << std::endl;

  Stream << DumpField(WorkQueue) << std::endl;

  return Stream;
}

#endif
