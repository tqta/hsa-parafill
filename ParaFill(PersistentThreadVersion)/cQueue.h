#ifndef cQueue_h
#define cQueue_h

#include <atomic>
#include <cstddef>
#include <thread>
#include <vector>
#include <ostream>

template<class Type>
class cQueue
{
public:
  cQueue(void) : QueueSize(0u)
  {
    clear();

    return;
  }

  cQueue(size_t _QueueSize) : QueueSize(_QueueSize)
  {
    // Create the queue array.
    Queue.resize(QueueSize);

    clear();

    return;
  }

  ~cQueue(void)
  {
    Queue.clear();

    return;
  }

  inline void clear(void)
  {
    // Set queue management pointers.
    QueueFront =
      QueueRear =
      QueueTempRear = 0u;

    // Nothing is in the queue.
    nQueued = 0;

    DequeueCount = 0u;
    EnqueueCount = 0u;

    return;

  }

  inline int32_t GetnQueued(void) const {return nQueued;}

  inline void resize(size_t _QueueSize) { clear(); QueueSize = _QueueSize; Queue.resize(QueueSize); return;}

  void Enqueue(const Type &Node)
  {
    // Enqueue is non-blocking.  Exactly one thread will make progress.

    // Get the slot to store node by advancing QueueTempRear.
    auto OldRear = QueueTempRear++;

    // Store node.
    Queue[OldRear % QueueSize] = Node;

    // Make it visible to all threads.
    std::atomic_thread_fence(std::memory_order_seq_cst);

    // Several threads could be enqueuing in parallel.  Now that this threads
    // data is stored, the thread whose QueueRear == OldRear can advance QueueRear.
    // In this way, each parallel thread that is enqueueing will advance in its
    // turn.
    while (QueueRear.compare_exchange_strong(OldRear, OldRear + 1u))
    {
      std::this_thread::yield();
    }

    ++nQueued;
    ++EnqueueCount;

    return;
  }

  bool Dequeue(Type &Node)
  {
    // Dequeue is wait-free or lock-free.

    // Check for nodes.
    if (nQueued-- <= 0)
    {
      // No nodes to dequeue.  Correct count.
      ++nQueued;

      // Let user know no nodes available.
      return false;
    }

    // Get a node.
    Node = Queue[QueueFront++ % QueueSize];

    ++DequeueCount;

    return true;
  }

public:
  size_t QueueSize;
  std::atomic<size_t> EnqueueCount;
  std::atomic<size_t> DequeueCount;
  std::vector<Type> Queue;
  std::atomic<uint32_t> QueueFront;
  std::atomic<uint32_t> QueueRear;
  std::atomic<uint32_t> QueueTempRear;
  std::atomic<int32_t> nQueued;
};


#define DumpField(Field) #Field "=" << Context.Field
#define DumpHexField(Field) #Field "=" << Hex(Context.Field)
#define DumpAddrField(Field) #Field "=" << Addr(Context.Field)
#define DumpStructField(Field) #Field "(" << Addr(&Context.Field) << ") [" << Context.Field << "]"
template<class Type>
inline std::ostream &operator << (std::ostream &Stream, const cQueue<Type> &Context)
{
  Stream << DumpField(QueueSize) << std::endl;
  Stream << DumpField(QueueFront) << std::endl;
  Stream << DumpField(QueueRear) << std::endl;
  Stream << DumpField(QueueTempRear) << std::endl;
  Stream << DumpField(nQueued) << std::endl;
  Stream << DumpField(EnqueueCount) << std::endl;
  Stream << DumpField(DequeueCount) << std::endl;
  for (auto i = Context.QueueFront.load(); i < Context.QueueRear; ++i)
  {
    Stream << "  Q[" << i << "]: " << Context.Queue[i] << std::endl;
  }

  return Stream;
}
#undef DumpField
#undef DumpHexField
#undef DumpAddrField
#undef DumpStructField

#endif
