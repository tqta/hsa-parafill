#ifndef sSpanner_h
#define sSpanner_h

#include <ostream>

#include "UtilityFunctions.h"

typedef struct sSpanner
{
  const char *ID;
  struct sSpanner *Perp1;
  struct sSpanner *Perp2;
  int xInc;
  int yInc;
  uint32_t FillColor;
} tSpanner;

#define DumpField(Field) #Field "=" << Spanner.Field
#define DumpHexField(Field) #Field "=" << Hex(Spanner.Field)
#define DumpAddrField(Field) #Field "=" << Addr(Spanner.Field)
inline std::ostream &operator << (std::ostream &Stream, const tSpanner &Spanner)
{
  Stream << DumpField(ID) << ", " << DumpAddrField(Perp1) << ", " << DumpAddrField(Perp2) << ", " << DumpField(xInc) << ", " << DumpField(yInc);
  return Stream;
}
#undef DumpField
#undef DumpHexField
#undef DumpAddrField
#endif
