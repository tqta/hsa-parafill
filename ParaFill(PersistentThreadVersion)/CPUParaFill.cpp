#include "CPUParaFill.h"

#include <thread>

#include "cGPUExecutionContext.h"
#include "cQueue.h"
#include "cTimer.h"

#include "cCPUExecutionContext.h"
#include "UtilityFunctions.h"

static tCPUExecutionContext Parms;

// For debugging.
#include "CImg.h"
#include <vector>
static CImg<unsigned char> CorrectImg("./CorrectImage.bmp");

double CPUParaFillRun(cConfiguration &Configuration, const std::vector<uint32_t> &MasterImage,
    const CImg<unsigned char> &InputImage);
void CPUParaFillThread(uint32_t ThreadID);

void CPUParaFill(cConfiguration &Configuration, const std::vector<uint32_t> &MasterImage,
    const CImg<unsigned char> &InputImage)
{
#ifdef __Debug__
  // For debugging and testing.
  system("rm ./Error-*.png");
#endif

  Parms.Width = InputImage.width();
  Parms.Height = InputImage.height();
  Parms.nPixels = Parms.Width * Parms.Height;

  Parms.WorkQueue.resize(3 * Parms.nPixels);

  // Set the fill color with a flag that will never match an interior color.
  Parms.FillColor = Configuration.FillColor.ToUInt() | 0xff000000u;

  // Get interior color.
  Parms.InteriorColor = MasterImage[Index(Configuration.StartingPoints[0].x, Configuration.StartingPoints[0].y, Parms.Width)];

  Parms.nThreadsToUse = Configuration.nThreads;
  Parms.nThreadsActive = 0u;

  // Create spanners.
  Parms.LeftSpanner =
      {
          ID    : "Left",
          Perp1 : &Parms.UpSpanner,
          Perp2 : &Parms.DownSpanner,
          xInc  : -1,
          yInc  : 0,
          FillColor: 0xffFF0000,
      };

  Parms.RightSpanner =
      {
          ID    : "Right",
          Perp1 : &Parms.UpSpanner,
          Perp2 : &Parms.DownSpanner,
          xInc : 1,
          yInc : 0,
          FillColor: 0xff00FF00,
      };

  Parms.UpSpanner =
      {
          ID    : "Up",
          Perp1 : &Parms.LeftSpanner,
          Perp2 : &Parms.RightSpanner,
          xInc : 0,
          yInc : -1,
          FillColor: 0xff0000FF,
      };

  Parms.DownSpanner =
      {
          ID    : "Down",
          Perp1 : &Parms.LeftSpanner,
          Perp2 : &Parms.RightSpanner,
          xInc : 0,
          yInc : 1,
          FillColor: 0xff00FFFF,
      };

  auto SumExecutionTime = 0.0;
  for (auto Iteration = 0u; Iteration < Configuration.nIterations; ++Iteration)
  {
    SumExecutionTime += CPUParaFillRun(Configuration, MasterImage, InputImage);
  }

  CImg<unsigned char> OutputImage;
  OutputImage = InputImage;
  auto PixelIndex = 0u;
  for (auto y = 0; y < Parms.Height; ++y)
  {
    for (auto x = 0; x < Parms.Width; ++x)
    {
      auto Pixel = Parms.Image[PixelIndex++];
      OutputImage(x, y, 0, 2) = Pixel & 0xffu;
      Pixel >>= 8u;
      OutputImage(x, y, 0, 1) = Pixel & 0xffu;
      Pixel >>= 8u;
      OutputImage(x, y, 0, 0) = Pixel & 0xffu;
    }
  }

  OutputImage.save(Configuration.CPUOutputFilename);

  Configuration.StatsOut() << "CPUExecution=" << SumExecutionTime / double(Configuration.nIterations) << std::endl;
  Configuration.StatsOut() << "nEnqueued=" << Parms.WorkQueue.EnqueueCount << std::endl;
  Configuration.StatsOut() << "nDequeued=" << Parms.WorkQueue.DequeueCount << std::endl;

  auto ThreadID = 0u;
  size_t nCycles = 0u;
  for (const auto &CycleCounter : Parms.Cycles)
  {
    nCycles += CycleCounter;
    Configuration.StatsOut() << "nCycles[" << ThreadID++ << "]=" << CycleCounter << std::endl;
  }

  Configuration.StatsOut() << "TotalCycles=" << nCycles << std::endl;

  return;
}

double CPUParaFillRun(cConfiguration &Configuration, const std::vector<uint32_t> &MasterImage,
    const CImg<unsigned char> &InputImage)
{
  // Restore image.
  Parms.Image = MasterImage;

  // Reset counters.
  Parms.Cycles.resize(Configuration.nThreads);
  for (auto &Cycle : Parms.Cycles)
  {
    Cycle = 0u;
  }

  // Reset queue.
  Parms.WorkQueue.resize(3 * Parms.nPixels);

  // Add starting points to work queue.
  for (const auto &StartingPoint : Configuration.StartingPoints)
  {
    // Place first spanner in queue.
    Parms.WorkQueue.Enqueue({Spanner:&Parms.RightSpanner, x:StartingPoint.x, y:StartingPoint.y});
    if (StartingPoint.x > 0)
    {
      auto LeftPixel = Parms.Image[Index(StartingPoint.x - 1, StartingPoint.y, Parms.Width)];
      if (LeftPixel == Parms.InteriorColor)
      {
        // Place second spanner in queue.
        Parms.WorkQueue.Enqueue({Spanner:&Parms.LeftSpanner, x:StartingPoint.x - 1, y:StartingPoint.y});
      }
    }
  }

  // Crank up the threads.
  std::vector<std::thread *> Threads;

  // Start a timer.
  cTimer<> ExecutionTime;

  for (auto i = 0u; i < Parms.nThreadsToUse; ++i)
  {
    Threads.push_back(new std::thread(CPUParaFillThread, i));
  }

  // Wait for threads to finish.

  while (Threads.size())
  {
    // Get a thread;
    std::thread *Thread = Threads.back();

    // Remove it from queue.
    Threads.pop_back();

    // Wait for thread to end.
    Thread->join();

    // Delete it.
    delete Thread;
  }

  // For debugging and testing.
  CImg<unsigned char> OutputImage(Parms.Width, Parms.Height, 1, 3);
  auto PixelIndex = 0u;
  for (auto y = 0; y < Parms.Height; ++y)
  {
    for (auto x = 0; x < Parms.Width; ++x)
    {
      auto Pixel = Parms.Image[PixelIndex++];
      OutputImage(x, y, 0, 2) = Pixel & 0xffu;
      Pixel >>= 8u;
      OutputImage(x, y, 0, 1) = Pixel & 0xffu;
      Pixel >>= 8u;
      OutputImage(x, y, 0, 0) = Pixel & 0xffu;
    }
  }

  static auto ErrorCount = 0;
  if (OutputImage != CorrectImg)
  {
    std::cerr << "***************************Bad output image.***************************" << std::endl;
    OutputImage.save("./Error-.png", ++ErrorCount);
  }

  return ExecutionTime.GetDuration();

}

#define InBounds(x, y) ((x >= 0) && (x < Parms.Width) && (y >= 0) && (y < Parms.Height))

void CPUParaFillThread(uint32_t ThreadID)
{
//  std::cout << "Thread started." << std::endl;

  // Loop while there is work.
  while (Parms.WorkQueue.nQueued || Parms.nThreadsActive.load())
  {
    tWorkToken WorkToken;

    // Try to get some work.
    if (Parms.WorkQueue.Dequeue(WorkToken))
    {
      // We got work, so this thread is active.
      ++Parms.nThreadsActive;

      // Get span start.
      auto x = WorkToken.x;
      auto y = WorkToken.y;

      auto Perp1 = WorkToken.Spanner->Perp1;
      auto Perp2 = WorkToken.Spanner->Perp2;

      auto xInc = WorkToken.Spanner->xInc;
      auto yInc = WorkToken.Spanner->yInc;

      auto xInc1 = Perp1->xInc;
      auto yInc1 = Perp1->yInc;

      auto xInc2 = Perp2->xInc;
      auto yInc2 = Perp2->yInc;

      // Do a span.
      while (true)
      {
        // Count this cycle.
        ++Parms.Cycles[ThreadID];

        // Remember pixel for speed.
        auto &Pixel = Parms.Image[Index(x, y, Parms.Width)];
        if (InBounds(x, y) && (Pixel == Parms.InteriorColor))
        {
          // We have an interior color.  Mark it as filled.
          Pixel = Parms.FillColor;

          // Ensure other threads see it ASAP.
          std::atomic_thread_fence(std::memory_order_seq_cst);

          auto xx = x + xInc1;
          auto yy = y +yInc1;

          if (InBounds(xx, yy) && (Parms.Image[Index(xx, yy, Parms.Width)] == Parms.InteriorColor))
          {
            // Add perpendicular spanners to the work queue.
            Parms.WorkQueue.Enqueue({Spanner:Perp1, x:xx, y:yy});
          }

          xx = x + xInc2;
          yy = y + yInc2;

          if (InBounds(xx, yy) && (Parms.Image[Index(xx, yy, Parms.Width)] == Parms.InteriorColor))
          {
            // Add perpendicular spanners to the work queue.
            Parms.WorkQueue.Enqueue({Spanner:Perp2, x:xx, y:yy});
          }

          // Move along span.
          x += xInc;
          y += yInc;
        }
        else
        {
          // Span is over.
          break;
        }
      }
      // Show thread is no longer active.
      --Parms.nThreadsActive;
    }
  }

//  std::cout << "Thread end." << std::endl;

  // All thread inactive and no work queued -- we're done.
  return;
}
