#ifndef GPUParaFill_h
#define GPUParaFill_h

#include <vector>

#include "APUHelper.h"
#include "cConfiguration.h"
#include "cGPUExecutionContext.h"

void GPUParaFill(sOpenCLConfiguration &OpenCLConfiguration, cConfiguration &Configuration,
     const std::vector<uint32_t> &MasterImage, const CImg<unsigned char> &InputImage);

#endif
