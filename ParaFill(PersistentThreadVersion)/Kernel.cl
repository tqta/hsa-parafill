#ifndef __BlockingFactor__
#define __BlockingFactor__ 1
#endif

// Right now only blocking factor of 1 supported.
#undef __BlockingFactor__
#define __BlockingFactor__ 1

typedef unsigned int uint32_t;

#define Load(x) (*((volatile uint32_t *) &x))
#define Acquire(x) atomic_load_explicit((volatile atomic_uint *)&x, memory_order_acquire)

#define InBounds(x, y) ((x >= 0) && (x < Width) && (y >= 0) && (y < Height))

#define FALSE 0
#define TRUE 1

// This should be set to the wavefront size.
const uint32_t gWaveFrontSize = 64u;

typedef struct sThreadDebugInfo
{
  size_t CycleCount;
  size_t StarvedCycleCount;
  size_t SpannerCount;
} tThreadDebugInfo;

typedef struct sSpanner
{
  __global const char *ID;__global
  struct sSpanner *Perp1;
  __global struct sSpanner *Perp2;
  int xInc;
  int yInc;
  uint32_t FillColor;
} tSpanner;

typedef struct sWorkToken
{
  __global tSpanner *Spanner;
  int x;
  int y;
} tWorkToken;

typedef struct sGPUExecutionContext
{
  // CAS operations on global memory require the expected value in global memory.
  // There is one per workgroup, and it used only by the master thread.
//  __global uint32_t *OldQueueFront;
  __global uint32_t *ThreadTempExpected;

  __global uint32_t *Image;
  __global tWorkToken *WorkQueue;
  tSpanner LeftSpanner;
  tSpanner RightSpanner;
  tSpanner UpSpanner;
  tSpanner DownSpanner;
  int Width;
  int Height;
  int nPixels;
  uint32_t WorkgroupSize;
  uint32_t nWorkgroups;
  uint32_t nThreads;

  // Colors are stored 0xxxRRGGBB
  uint32_t InteriorColor;
  uint32_t FillColor;

  atomic_uint nThreadsWorking;

  atomic_uint nQueued;
#ifdef __Debug__
  atomic_uint nEnqueued;
  atomic_uint nDequeued;
#endif

  uint32_t QueueSize;
  atomic_uint WorkQueueFront;
  atomic_uint WorkQueueRear;
  atomic_uint WorkQueueTempRear;

#ifdef __Debug__
  // Debugging stuff goes here.
  uint32_t CyclesDone;
  tThreadDebugInfo *ThreadDebugInfo;
#endif

  double Buffer[0];
} tGPUExecutionContext;

__kernel void PartitioningParaFill(__global tGPUExecutionContext *Parms)
{
  // ****************************************************************************************************
  // Initialize.
  // ****************************************************************************************************

  // Get identity of persistent thread.  This has no direct correlation to work assignment.
  // It simply identifies a thread that can be assigned work.

  uint32_t LocalID = get_local_id(0);
  uint32_t GlobalID = get_global_id(0);

  // Master thread enqueues and dequeues work on behalf of entire wavefront.
  bool IsMasterThread = (LocalID == 0u);

  // Set when this thread needs new work.  Initially all threads need work.
  bool ThreadNeedsNewWork = true;

  // Work offset for thread.  Used to distribute work.
  uint32_t WorkOffset;

  // Thread's work token is saved here.
  tWorkToken ThreadWorkToken;

  // New work token is saved here.
  // Each thread can discover at most two new work tokens per block per cycle.
  tWorkToken ThreadNewWorkTokens[__BlockingFactor__ * 2u];

  // Number of new work tokens discovered in this cycle.
  uint32_t ThreadNewWorkCount = 0u;
  uint32_t ThreadNewWorkIndex = 0u;

  // Height/Width of image.
  int Width = Parms->Width;
  int Height = Parms->Height;

  // Interior/Fill color
  uint32_t InteriorColor = Parms->InteriorColor;
  uint32_t FillColor = Parms->FillColor;

  int x;
  int y;
  uint32_t PixelIndex;

  __local uint32_t lBlockCount;

  // Old head stored here while dequeuing new work.
  __local uint32_t lOldFront;

  // Stores the work available.
  __local uint32_t lnWorkAvailableInQueue;

  // Flags if work actually dequeued.
  __local bool lGotWork;

  // Stores the number of threads needing work in wavefront.
  __local uint32_t lnThreadsNeedingWork;

  // Stores the work actually dequeued this cycle.
  __local uint32_t lnWorkActuallyDequeued;

  // When current rear == old rear, it is safe to update current rear.
  __local uint32_t lOldRear;

  // This contains the number of new work tokens discovered in the last pass.  Initially there are none.
  __local uint32_t lnDiscoveredWork;

  __local uint32_t lInsertIndex;
  __local uint32_t lDequeueIndex;

  __local uint32_t lNeedToSwingTail;

  if (IsMasterThread)
  {
    lnThreadsNeedingWork = Parms->WorkgroupSize;
    lnDiscoveredWork = 0u;
    lNeedToSwingTail = false;
    lGotWork = false;
  }

  // ****************************************************************************************************
  // Master Work Loop
  // ****************************************************************************************************

  // The only correct bounds of the work queue are WorkQueueFront and WorkQueueRear.  Since nQueued is
  // not updated at the same instant as WorkQueueFront and WorkQueueRear, it is only an estimate of the
  // number of elements in the queue.  However, if there are no working threads, nQueued is accurate
  // and thus good enough for checking if all work complete.

  // Loop until all work done.
#ifdef __Debug__
  // Debug mode cycle limits.
  while ((Load(Parms->nThreadsWorking) || Load(Parms->nQueued)) && (Parms->CyclesDone < 100000u))
#else
  while (Load(Parms->nThreadsWorking) || Load(Parms->nQueued))
#endif
  {

    // ****************************************************************************************************
    // Step 1 -- Attempt parallel dequeue of work tokens for all starved threads in wavefront.
    // ****************************************************************************************************
    // Attempt to get work for starved threads.
    // Failure has a relatively low cost because it simply retries in the next cycle.

    // We need to ensure we have the latest copy of the WorkQueue data in Parms in global memory.
    // This is ensured by the sequential consistency fence in the loop epilog.
//    atomic_work_item_fence (CLK_GLOBAL_MEM_FENCE, memory_order_acquire, memory_scope_device);

    if (IsMasterThread)
    {
      if (lnThreadsNeedingWork)
      {
        // This is the master thread and at least one thread in the wavefront needs work.
        // Remember the old front of the queue.
        lOldFront = Acquire(Parms->WorkQueueFront);

        // Note that other threads may have advanced WorkQueueRear, but the only effect is that
        // lnWorkAvailableInQueue is underestimated and we dequeue less than we could have.
        // Do not used nQueued -- it briefly overestimates the number of elements queued.
        lnWorkAvailableInQueue = Acquire(Parms->WorkQueueRear) - Acquire(Parms->WorkQueueFront);

        // Find out how many WorkTokens we can actually dequeue.
        lnWorkActuallyDequeued = (lnThreadsNeedingWork < lnWorkAvailableInQueue) ? lnThreadsNeedingWork : lnWorkAvailableInQueue;

        // Swing the front of the queue to its new value if no other thread has changed front.
        Parms->ThreadTempExpected[GlobalID] = lOldFront;
        lGotWork = atomic_compare_exchange_strong_explicit((volatile __global atomic_uint *) &Parms->WorkQueueFront,
            Parms->ThreadTempExpected + GlobalID, Load(Parms->WorkQueueFront) + lnWorkActuallyDequeued,
            memory_order_acq_rel, memory_order_relaxed, memory_scope_device);

        // We use this index to load the work items.
        lDequeueIndex = 0u;
      }
      else
      {
        lGotWork = false;
      }
    }

    // All threads participate in the next step.  We must ensure they have the current value of lGotWork.
    // Only local sequential consistency is required.
    atomic_work_item_fence (CLK_LOCAL_MEM_FENCE, memory_order_seq_cst, memory_scope_work_group);

    // Distribute work to threads needing work in parallel.
    if (lGotWork && ThreadNeedsNewWork)
    {
      WorkOffset = atomic_inc(&lDequeueIndex);

      if (WorkOffset < lnWorkActuallyDequeued)
      {
        // This thread actually got work.  Set its work token.
        ThreadWorkToken = Parms->WorkQueue[(lOldFront + WorkOffset) % Parms->QueueSize];
        ThreadNeedsNewWork = false;
        x = ThreadWorkToken.x;
        y = ThreadWorkToken.y;
#ifdef __Debug__
        ++Parms->ThreadDebugInfo[GlobalID].SpannerCount;
#endif
      }
    }

    // Adjust number of threads needing work.
    if (IsMasterThread && lGotWork)
    {
      // Count Dequeues.
#ifdef __Debug__
      atomic_add((volatile __global uint32_t *)&Parms->nDequeued, lnWorkActuallyDequeued);
#endif

      // The order of the next two atomic adds is important.  To keep threads from dying off,
      // we must show the thread is working before we show it is dequeued.
      atomic_add((volatile __global uint32_t *)&Parms->nThreadsWorking, lnWorkActuallyDequeued);
      atomic_sub((volatile __global uint32_t *)&Parms->nQueued, lnWorkActuallyDequeued);

      lnThreadsNeedingWork -= lnWorkActuallyDequeued;
    }
    // ****************************************************************************************************
    // End Of Step 1.
    // ****************************************************************************************************

    // ****************************************************************************************************
    // Step 2 -- Do a partition if work is available.
    // ****************************************************************************************************

    // Do the work unit represented by ThreadWorkToken.
    if (!ThreadNeedsNewWork)
    {
#ifdef __Debug__
      // Thread is doing work this cycle.
      ++Parms->ThreadDebugInfo[GlobalID].CycleCount;
#endif

      // Do the work unit represented by ThreadWorkToken.

      // If new work is discovered, flag it so it will be queued at the top of the cycle.
      // This requires setting ThreadNewWorkTokens, atomically incrementing lnDiscoveredWork,
      // and setting ThreadDiscoveredWork.

      // When this unit of work is complete, if more work can be done advance ThreadWorkToken.
      // Otherwise flag thread as starved so that it dequeues work in next cycle.
      // This requires atomically incrementing lnThreadsNeedingWork and setting ThreadNeedsNewWork.

      // Lastly, an algorithm specific StillWorking() mechanism must be implemented.
      // It will be based on detecting when no more progress is globally possible.

      // ParaFill algorithm for a single cycle.
      // ThreadWorkToken contains the work to be performed.

      PixelIndex = (y * Width) + x;

      // Expect an interior color
      Parms->ThreadTempExpected[GlobalID] = InteriorColor;
      if ((InBounds(x, y)) &&
          atomic_compare_exchange_strong_explicit((volatile __global atomic_uint *) (Parms->Image + PixelIndex),
              (__global uint32_t *) (Parms->ThreadTempExpected + GlobalID), FillColor,
              memory_order_acq_rel, memory_order_relaxed, memory_scope_device))
      {
        // We have an interior pixel that has been changed to the fill color.

        // Attempt creating perpendicular spanners.

        // Move in the first perpendicular direction.
        tWorkToken NewWorkToken;
        NewWorkToken.Spanner = ThreadWorkToken.Spanner->Perp1;
        NewWorkToken.x = x + NewWorkToken.Spanner->xInc;
        NewWorkToken.y = y + NewWorkToken.Spanner->yInc;

        // Get pixel index of starting point.
        int xx = NewWorkToken.x;
        int yy = NewWorkToken.y;
        uint32_t PerpPixelIndex = (yy * Width) + xx;
        if ((InBounds(xx, yy)) && (Parms->Image[PerpPixelIndex] == InteriorColor))
        {
          // The new perpendicular pixel is connected.  Add it as work.
          ThreadNewWorkTokens[ThreadNewWorkCount++] = NewWorkToken;
        }

        // Move in the second perpendicular direction.
        NewWorkToken.Spanner = ThreadWorkToken.Spanner->Perp2;
        NewWorkToken.x = x + NewWorkToken.Spanner->xInc;
        NewWorkToken.y = y + NewWorkToken.Spanner->yInc;

        // Get pixel index of starting point.
        xx = NewWorkToken.x;
        yy = NewWorkToken.y;
        PerpPixelIndex = (yy * Width) + xx;
        if ((InBounds(xx, yy)) && (Parms->Image[PerpPixelIndex] == InteriorColor))
        {
          // The new perpendicular pixel is connected.  Add it as work.
          ThreadNewWorkTokens[ThreadNewWorkCount++] = NewWorkToken;
        }

        // Include this work in the count
        atomic_add(&lnDiscoveredWork, ThreadNewWorkCount);

        // Move to next ThreadWorkToken.
        x += ThreadWorkToken.Spanner->xInc;
        y += ThreadWorkToken.Spanner->yInc;
      }
      else
      {
        ThreadNeedsNewWork = true;
        atomic_inc(&lnThreadsNeedingWork);
        atomic_dec((volatile __global unsigned int *) (&Parms->nThreadsWorking));
      }
    }
    else
    {
#ifdef __Debug__
      ++Parms->ThreadDebugInfo[GlobalID].StarvedCycleCount;
#endif
    }

    // ****************************************************************************************************
    // End Of Step 2.
    // ****************************************************************************************************

    // ****************************************************************************************************
    // Step 3 -- Highly parallel enqueue of all newly discovered work in wavefront.
    // ****************************************************************************************************
    // Enqueue work discovered in last step.  This ensures any newly discovered work is
    // available to dequeue for starving threads.
    // Here is the strategy.  When each thread discovers new work, it creates a WorkToken
    // for that item and:
    // 1) Adds the WorkToken to the thread's discovered work.
    // 2) Increments the workgroup's count of discovered work (lnDiscoveredWork).
    // The technique must ensure the actual QueueRear is not updated until after all newly
    // discovered work is stored in the queue.

    if (IsMasterThread && lnDiscoveredWork)
    {
      // There is discovered work.  Make room for it at end of queue.
      // There is a phasing problem here.  The rear cannot be advanced
      // until the new work is actually saved and ready for dequeueing.
      // So in the first phase, a temporary rear pointer is advanced.
      // Each thread remembers the old rear it is using, which becomes the base index
      // for storing data.
      // This can be done by multiple wavefronts before data is actually saved,
      // but each wavefont has its own rear.

      // Advance TempRear and remember old value.  The old value is the base where
      // this workgroup stores its new work.
      // In effect this reserves space in the queue for this workgroup's discovered data.
      lOldRear = atomic_add((volatile __global uint32_t *)&Parms->WorkQueueTempRear, lnDiscoveredWork);

#ifdef __Debug__
      // Count the parallel enqueues.
      atomic_add((volatile __global uint32_t *)&Parms->nEnqueued, lnDiscoveredWork);
#endif

      // Start saving at beginning of reserved area.
      lInsertIndex = 0u;

      // Show we need to swing the tail.
      lNeedToSwingTail = true;
    }

    // All threads discovering new work must now add their work in parallel.
    if (ThreadNewWorkCount)
    {
      ThreadNewWorkIndex = 0u;
      while (ThreadNewWorkIndex < ThreadNewWorkCount)
      {
        Parms->WorkQueue[(lOldRear + atomic_inc((volatile __local uint32_t *) &lInsertIndex)) % Parms->QueueSize] =
        ThreadNewWorkTokens[ThreadNewWorkIndex++];
      }

      // All new work has been stored.
      ThreadNewWorkCount = 0u;
    }

    // Now that after the new work tokens are saved, the rear pointer can be advanced.
    // However, only the wavefront whose temp rear index equals the current rear
    // index can advance.  The other wavefronts must wait their turn, which comes
    // in another cycle.
    // Swing tail if it is this wavefronts turn to do so.
    if (IsMasterThread && lNeedToSwingTail)
    {
      // This swings the tail and does a back off if swing fails.
      // The do loop is essentially a back off because workgroups are delayed until
      // it is their turn to patch WorkQueueRear.
      do
      {
        // Swing tail if is this core's turn.
        Parms->ThreadTempExpected[GlobalID] = lOldRear;
        if (atomic_compare_exchange_strong_explicit((volatile __global atomic_uint *) &Parms->WorkQueueRear,
                Parms->ThreadTempExpected + GlobalID, lOldRear + lnDiscoveredWork,
                memory_order_acq_rel, memory_order_relaxed, memory_scope_device))
        {
          // All discovered work is stored.
          atomic_add((volatile __global uint32_t *)&Parms->nQueued, lnDiscoveredWork);

          lnDiscoveredWork = 0u;

          // Tail has been swung.
          lNeedToSwingTail = false;
        }
      } while (lNeedToSwingTail);
    }

    // ****************************************************************************************************
    // End Of Step 3.
    // ****************************************************************************************************

#ifdef __Debug__
    // For debugging, increment cycle counter.
    if (IsMasterThread)
    {
      atomic_inc(&Parms->CyclesDone);
    }
#endif

    // ****************************************************************************************************
    // Loop epilog.
    // ****************************************************************************************************

    // Ensure all changes are visible for the loop test.
    atomic_work_item_fence (CLK_GLOBAL_MEM_FENCE | CLK_LOCAL_MEM_FENCE, memory_order_seq_cst, memory_scope_device);
  }

  return;
}

__kernel void WorkSharingParaFill(__global tGPUExecutionContext *Parms)
{
  // TODO:  This needs to be done on campus due to the likely deadlocks during development.

  return;
}
