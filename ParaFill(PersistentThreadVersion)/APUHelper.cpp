#include "APUHelper.h"

#include <string.h>
#include <iostream>
#include <fstream>

#define MAX_BUFFER_SIZE 1024

// clReleaseDevice is not supported -- fake it
#define clReleaseDevice(rc) CL_SUCCESS

const char *OpenCLErrors[] =
{
  "CL_SUCCESS",
  "CL_DEVICE_NOT_FOUND",
  "CL_DEVICE_NOT_AVAILABLE",
  "CL_COMPILER_NOT_AVAILABLE",
  "CL_MEM_OBJECT_ALLOCATION_FAILURE",
  "CL_OUT_OF_RESOURCES",
  "CL_OUT_OF_HOST_MEMORY - OpenCL could not allocate resources.",
  "CL_PROFILING_INFO_NOT_AVAILABLE",
  "CL_MEM_COPY_OVERLAP",
  "CL_IMAGE_FORMAT_MISMATCH",
  "CL_IMAGE_FORMAT_NOT_SUPPORTED",
  "CL_BUILD_PROGRAM_FAILURE",
  "CL_MAP_FAILURE",
  "CL_MISALIGNED_SUB_BUFFER_OFFSET",
  "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_UNKNOWN",
  "CL_INVALID_VALUE - A parameter has an invalid value.",
  "CL_INVALID_DEVICE_TYPE",
  "CL_INVALID_PLATFORM - Platform is not valid.",
  "CL_INVALID_DEVICE",
  "CL_INVALID_CONTEXT",
  "CL_INVALID_QUEUE_PROPERTIES",
  "CL_INVALID_COMMAND_QUEUE",
  "CL_INVALID_HOST_PTR",
  "CL_INVALID_MEM_OBJECT",
  "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",
  "CL_INVALID_IMAGE_SIZE",
  "CL_INVALID_SAMPLER",
  "CL_INVALID_BINARY",
  "CL_INVALID_BUILD_OPTIONS",
  "CL_INVALID_PROGRAM",
  "CL_INVALID_PROGRAM_EXECUTABLE",
  "CL_INVALID_KERNEL_NAME",
  "CL_INVALID_KERNEL_DEFINITION",
  "CL_INVALID_KERNEL",
  "CL_INVALID_ARG_INDEX",
  "CL_INVALID_ARG_VALUE",
  "CL_INVALID_ARG_SIZE",
  "CL_INVALID_KERNEL_ARGS",
  "CL_INVALID_WORK_DIMENSION",
  "CL_INVALID_WORK_GROUP_SIZE",
  "CL_INVALID_WORK_ITEM_SIZE",
  "CL_INVALID_GLOBAL_OFFSET",
  "CL_INVALID_EVENT_WAIT_LIST",
  "CL_INVALID_EVENT",
  "CL_INVALID_OPERATION",
  "CL_INVALID_GL_OBJECT",
  "CL_INVALID_BUFFER_SIZE",
  "CL_INVALID_MIP_LEVEL",
  "CL_INVALID_GLOBAL_WORK_SIZE",
  "CL_INVALID_PROPERTY"
};

size_t nOpenCLErrors = sizeof(OpenCLErrors) / sizeof(const char *);
sOpenCLConfiguration::sOpenCLConfiguration
(
    const std::string         _KernelFilename       ,
          cl_uint             _PlatformIndex        ,
          cl_uint             _DeviceIndex          ,
          bool                _Verbose              ,
          bool                _UseAtomics           ,
          bool                _UseFineGrainedBuffers,
          cl_queue_properties _CommandQueueProperties,
          const std::string   &_CompileFlags
)
{
  // Remember config parms.
  KernelFilename        = _KernelFilename;
  PlatformIndex         = _PlatformIndex;
  DeviceIndex           = _DeviceIndex;
  Verbose               = _Verbose;
  UseAtomics            = _UseAtomics;
  UseFineGrainedBuffers = _UseFineGrainedBuffers;

  cl_int rc = CL_SUCCESS;

  // Get the number of platforms.
  rc = clGetPlatformIDs(cl_uint(0), NULL, &nPlatforms);
  HandleOpenCLError(rc, "clGetPlatformIDs", __FILE__, __LINE__);

  if (_Verbose)
  {
    std::cout << "Got # of Platforms." << std::endl;
  }

  // Get the platform IDs.
  PlatformIDs.resize(nPlatforms);
  rc = clGetPlatformIDs(nPlatforms, PlatformIDs.data(), NULL);
  HandleOpenCLError(rc, "clGetPlatformIDs", __FILE__, __LINE__);

  if (_Verbose)
  {
    std::cout << "Got Platform IDs" << std::endl;

    for (unsigned int i = 0u; i < nPlatforms; ++i)
    {
      std::cout << "Platform " << i << ": " << GetPlatformInfoString(PlatformIDs[i], CL_PLATFORM_NAME) << std::endl;
    }
  }

  // Get selected platorm.
  PlatformID = PlatformIDs[_PlatformIndex];

  if (_Verbose)
  {
    std::cout << "Using platform: " << GetPlatformInfoString(PlatformID, CL_PLATFORM_NAME) << std::endl;
  }

  // Get the number of devices on the focus platform.
  rc = clGetDeviceIDs(PlatformID, CL_DEVICE_TYPE_ALL, cl_uint(0), NULL, &nDevices);
  HandleOpenCLError(rc, "clGetDeviceIDs", __FILE__, __LINE__);

  if (_Verbose)
  {
    std::cout << "Got # of devices for selected platform." << std::endl;
  }

  // Get the device IDs on the focus platform.
  DeviceIDs.resize(nDevices);
  rc = clGetDeviceIDs(PlatformID, CL_DEVICE_TYPE_ALL, nDevices, DeviceIDs.data(), NULL);
  HandleOpenCLError(rc, "clGetDeviceIDs", __FILE__, __LINE__);

  DeviceID = DeviceIDs[_DeviceIndex];
  cl_device_id DeviceList[] = { DeviceID };

  if (_Verbose)
  {
    std::cout << "Got devices for selected platform." << std::endl;

    for (unsigned int i = 0u; i < nDevices; ++i)
    {
      std::cout << "Device " << i << ": " << GetDeviceInfoString(DeviceIDs[i], CL_DEVICE_NAME) << std::endl;
    }

    std::cout << "Using device: " << GetDeviceInfoString(DeviceID, CL_DEVICE_NAME) << std::endl;

    // Print device info
    std::cout << "                 DEVICE_NAME: " << GetDeviceInfoString    (CL_DEVICE_NAME                 ) << std::endl;
    std::cout << "               DEVICE_VENDOR: " << GetDeviceInfoString    (CL_DEVICE_VENDOR               ) << std::endl;
    std::cout << "              DEVICE_VERSION: " << GetDeviceInfoString    (CL_DEVICE_VERSION              ) << std::endl;
    std::cout << "              DRIVER_VERSION: " << GetDeviceInfoString    (CL_DRIVER_VERSION              ) << std::endl;
    std::cout << "    DEVICE_MAX_COMPUTE_UNITS: " << GetDeviceInfo<cl_uint> (CL_DEVICE_MAX_COMPUTE_UNITS    ) << std::endl;
    std::cout << "  DEVICE_MAX_CLOCK_FREQUENCY: " << GetDeviceInfo<cl_uint> (CL_DEVICE_MAX_CLOCK_FREQUENCY  ) << std::endl;
    std::cout << "      DEVICE_GLOBAL_MEM_SIZE: " << GetDeviceInfo<cl_ulong>(CL_DEVICE_GLOBAL_MEM_SIZE      ) << std::endl;
    std::cout << "       DEVICE_LOCAL_MEM_SIZE: " << GetDeviceInfo<cl_ulong>(CL_DEVICE_LOCAL_MEM_SIZE       ) << std::endl;
    std::cout << "DEVICE_GLOBAL_MEM_CACHE_SIZE: " << GetDeviceInfo<size_t>  (CL_DEVICE_GLOBAL_MEM_CACHE_SIZE) << std::endl;
    std::cout << "  DEVICE_MAX_WORK_GROUP_SIZE: " << GetDeviceInfo<size_t>  (CL_DEVICE_MAX_WORK_GROUP_SIZE  ) << std::endl;
  }

  // Ensure fine grained buffer support is available.
  cl_device_svm_capabilities DEVICE_CAPS = GetDeviceInfo<cl_device_svm_capabilities>(CL_DEVICE_SVM_CAPABILITIES);

  if (_UseFineGrainedBuffers)
  {
    if (!(DEVICE_CAPS & CL_DEVICE_SVM_FINE_GRAIN_BUFFER))
    {
      throw "Fine grain buffer support not available.";
    }
  }

  if (_UseAtomics)
  {
    // Ensure atomics support is available
    if (!(DEVICE_CAPS & CL_DEVICE_SVM_ATOMICS))
    {
      throw "SVM Atomics support not available.";
    }
  }


  // Get max workgroup size for device.
  // size_t MaxWorkgroupSize = GetDeviceInfoSIZET(SelectedDevice, CL_DEVICE_MAX_WORK_GROUP_SIZE);

  // Create a context on the focus device.

  cl_context_properties ContextProperties[] = { CL_CONTEXT_PLATFORM, (cl_context_properties)(PlatformIDs[_PlatformIndex]), 0, 0 };

  Context = clCreateContext(ContextProperties, 1, DeviceList, NULL,  NULL, &rc);
  HandleOpenCLError(rc, "clCreateContext", __FILE__, __LINE__);

  if (_Verbose)
  {
    std::cout << "Created context on selected device." << std::endl;
  }

  // Create a command queue on context/device.
  cl_queue_properties CommandQueueProperties[] = { CL_QUEUE_PROPERTIES, _CommandQueueProperties, 0 }; //CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE
  CommandQueue = clCreateCommandQueueWithProperties(Context, DeviceID, CommandQueueProperties, &rc);
  HandleOpenCLError(rc, "clCreateCommandQueueWithProperties", __FILE__, __LINE__);

  if (_Verbose)
  {
    std::cout << "Created command queue on context and selected device." << std::endl;
  }

  // Create a program object.

  // Open a file stream for the kernel source.
  std::ifstream KernelFile(_KernelFilename.c_str());
  if (!KernelFile.is_open())
  {
    throw ("Could not open kernel source file \"" + _KernelFilename + "\" for input.").c_str();
  }

  // Read the entire kernel into a string.
  std::string KernelSource((std::istreambuf_iterator<char>(KernelFile)), std::istreambuf_iterator<char>());
//  std::cout << KernelSource << std::endl;

  KernelFile.close();

  const char *Source[] = {KernelSource.c_str()};
  cl_uint SourceCount = sizeof(Source) / sizeof(const char *);
  size_t SourceLengths[] = {KernelSource.length()};
  Program = clCreateProgramWithSource(Context, SourceCount, Source, SourceLengths, &rc);
  HandleOpenCLError(rc, "clCreateProgramWithSource", __FILE__, __LINE__);

  if (_Verbose)
  {
    std::cout << "Program created." << std::endl;
  }

  cl_device_id BuildDeviceList[] = { DeviceID };
  cl_uint NumBuildDevices = sizeof(BuildDeviceList) / sizeof(cl_device_id);

//  rc = clBuildProgram(Program, NumBuildDevices, BuildDeviceList, "-cl-std=CL2.0 -save-temps", NULL, NULL);
  rc = clBuildProgram(Program, NumBuildDevices, BuildDeviceList, _CompileFlags.c_str(), NULL, NULL);

  if (rc == CL_BUILD_PROGRAM_FAILURE)
  {
    static std::string Temp = "Compile failed:\n" + GetProgramInfoString(CL_PROGRAM_BUILD_LOG);
    throw (const char *)(Temp.c_str());
  }

  HandleOpenCLError(rc, "clBuildProgram", __FILE__, __LINE__);

  if (_Verbose)
  {
    std::cout << "Program compiled." << std::endl;
  }

  clFinish(CommandQueue);

  return;
}

sOpenCLConfiguration::~sOpenCLConfiguration(void)
{
  cl_int rc = CL_SUCCESS;

  rc = clReleaseProgram(Program);
  HandleOpenCLError(rc, "clReleaseProgram", __FILE__, __LINE__);

  if (Verbose)
  {
    std::cout << "Program freed." << std::endl;
  }

  rc = clReleaseCommandQueue(CommandQueue);
  HandleOpenCLError(rc, "clReleaseCommandQueue", __FILE__, __LINE__);

  if (Verbose)
  {
    std::cout << "CommandQueue freed." << std::endl;
  }

  rc = clReleaseContext(Context);
  HandleOpenCLError(rc, "clReleaseContext", __FILE__, __LINE__);

  if (Verbose)
  {
    std::cout << "Context freed." << std::endl;
  }

  // Release devices.  (Not supported in Multi2Sim, but see top of program for workaround.)
  for (int DeviceIndex = 0; DeviceIndex < int(nDevices); ++DeviceIndex)
  {
    rc = clReleaseDevice(Device[DeviceIndex]);
    HandleOpenCLError(rc, "clReleaseDevice", __FILE__, __LINE__);

    if (Verbose)
    {
      std::cout << "Device " << DeviceIndex << " freed." << std::endl;
    }
  }

  return;
}

cl_kernel sOpenCLConfiguration::CreateKernel(const std::string &KernelFunctionName)
{
  cl_int rc = CL_SUCCESS;

 // Create a kernel object
  cl_kernel Kernel = clCreateKernel(Program, KernelFunctionName.c_str(), &rc);
  HandleOpenCLError(rc, "clCreateKernel", __FILE__, __LINE__);

  if (Verbose)
  {
    std::cout << "Created kernel \"" << KernelFunctionName << "\"." << std::endl;
  }

  return Kernel;
}

std::string sOpenCLConfiguration::GetDeviceInfoString(cl_device_id &DeviceID, cl_device_info ParamName)
{
  cl_int rc = CL_SUCCESS;
  size_t ParmSize = 0u;
  rc = clGetDeviceInfo(DeviceID, ParamName, 0u, NULL, &ParmSize);
  HandleOpenCLError(rc, "clGetDeviceInfo", __FILE__, __LINE__);

  if (ParmSize == 0u)
  {
    ParmSize = 4096u;
  }

  char Temp[ParmSize];
  strcpy(Temp, "<No result>");

  rc = clGetDeviceInfo(DeviceID, ParamName, ParmSize, Temp, NULL);
  HandleOpenCLError(rc, "clGetDeviceInfo", __FILE__, __LINE__);

  std::string Parm;
  Parm = Temp;

  return Parm;
}

std::string sOpenCLConfiguration::GetPlatformInfoString(cl_platform_id &PlatformID, cl_platform_info ParamName)
{
  cl_int rc = CL_SUCCESS;
  size_t ParmSize = 0u;
  rc = clGetPlatformInfo(PlatformID, ParamName, 0u, NULL, &ParmSize);
  HandleOpenCLError(rc, "clGetPlatformInfo", __FILE__, __LINE__);

  if (ParmSize == 0u)
  {
    ParmSize = 4096u;
  }

  char Temp[ParmSize];
  strcpy(Temp, "<No result>");

  rc = clGetPlatformInfo(PlatformID, ParamName, ParmSize, Temp, NULL);
  HandleOpenCLError(rc, "clGetPlatformInfo", __FILE__, __LINE__);

  std::string Parm;
  Parm = Temp;

  return Parm;
}

std::string sOpenCLConfiguration::GetProgramInfoString(cl_program_info ParamName) const
{
  cl_int rc = CL_SUCCESS;
  size_t ParmSize;
  rc = clGetProgramBuildInfo (Program, DeviceID, ParamName, 0, NULL, &ParmSize);
  HandleOpenCLError(rc, "clGetProgramBuildInfo", __FILE__, __LINE__);
  char Text[ParmSize];
  rc = clGetProgramBuildInfo (Program, DeviceID, ParamName, ParmSize, Text, NULL);
  HandleOpenCLError(rc, "clGetProgramBuildInfo", __FILE__, __LINE__);

  return Text;
}

void sOpenCLConfiguration::HandleOpenCLError(cl_int rc, const char *Function, const char *File, int Line)
{
  static char ErrorBuffer[MAX_BUFFER_SIZE];
  if (rc == CL_SUCCESS)
  {
    // If no error return;
    return;
  }

  cl_uint ErrorIndex = -rc;
  const char *ErrorText = "UNKNOWN";

  if (ErrorIndex < nOpenCLErrors)
  {
    ErrorText = OpenCLErrors[ErrorIndex];
  }
  // Format an error message.
  sprintf(ErrorBuffer, "Error code: %d (%s), OpenCL function: %s, File: %s, Line: %d",
      rc, ErrorText, Function, File, Line);

  // Throw the error.
  throw(const char *) (ErrorBuffer);
}
