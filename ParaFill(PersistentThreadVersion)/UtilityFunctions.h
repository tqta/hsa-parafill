#ifndef UtilityFunctions_h
#define UtilityFunctions_h

// Utility functions

// (x,y) to index
inline unsigned int Index(unsigned int x, unsigned int y, unsigned int Width)
{
  return y * Width + x;
}

// Convert address to hex string.
inline std::string Addr(const void *Ad)
{
  std::ostringstream ss;
  ss << (void *) (((unsigned char *) Ad) - ((unsigned char *) nullptr));
  return ss.str();
}

// Convert size_t to hex string.
inline std::string Hex(size_t Val)
{
  return Addr((void *) Val);
}

#endif
