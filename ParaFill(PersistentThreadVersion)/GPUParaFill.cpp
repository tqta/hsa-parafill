#include "GPUParaFill.h"

#include <assert.h>

#include "cConfiguration.h"
#include "CImg.h"

// Execution context.
tGPUExecutionContext *Parms = nullptr;

const char *KernelFunctionNames[] =
  { "PartitioningParaFill", "WorkSharingParaFill" };
size_t nKernelFunctions = sizeof(KernelFunctionNames) / sizeof(const char *);

// For debugging.
#include "CImg.h"
#include <vector>
static CImg<unsigned char> CorrectImg("./CorrectImage.bmp");

// Align to next base.
inline size_t Align(size_t Val, size_t Base)
{
  return (Val % Base) ? Val + Base - (Val % Base) : Val;
}

double GPUParaFillCycle(sOpenCLConfiguration &OpenCLConfiguration, cl_kernel ParaFillKernel,
    const std::vector<uint32_t> &MasterImage, tGPUExecutionContext *Parms, const tConfiguration &Configuration);

void GPUParaFill(sOpenCLConfiguration &OpenCLConfiguration, cConfiguration &Configuration,
     const std::vector<uint32_t> &MasterImage, const CImg<unsigned char> &InputImage)
{
  int Width = InputImage.width();
  int Height = InputImage.height();
  int nPixels = Width * Height;

  size_t ConfigBaseSize = Align(sizeof(tGPUExecutionContext), 8u);
  size_t OldQueueFrontSize = Align(sizeof(uint32_t) * Configuration.nWorkgroups, 8u);
  size_t ThreadTempSize = Align(sizeof(uint32_t) * Configuration.nWorkgroups * Configuration.WorkgroupSize, 8u);
  size_t WorkQueueSize = Align(sizeof(tWorkToken) * nPixels * 3u, 8u);
  size_t ImageSize = Align(sizeof(tWorkToken) * nPixels, 8u);

#ifdef __Debug__
  size_t ThreadDebugCountSize = Align(sizeof(tThreadDebugInfo) * Configuration.nWorkgroups * Configuration.WorkgroupSize, 8u);

  size_t AllocatedSize = ConfigBaseSize +
      OldQueueFrontSize +
      ThreadTempSize +
      WorkQueueSize +
      ImageSize +
      ThreadDebugCountSize;
  std::cout << "AllocatedSize=" << AllocatedSize << std::endl;
#else
  size_t AllocatedSize = ConfigBaseSize +
      OldQueueFrontSize +
      ThreadTempSize +
      WorkQueueSize +
      ImageSize;
#endif

  // Get a bunch of coherent shared memory.  During initialization coherent data will be allocated in the pool.
  Parms = (tGPUExecutionContext *) OpenCLConfiguration.SVMAllocate<char>(AllocatedSize, CL_MEM_READ_WRITE);
  assert(Parms);
  std::memset(Parms, 0, AllocatedSize);

  Parms->QueueSize = nPixels * 3u;

#ifdef __Debug__
  std::cout << "Parms=" << Addr(Parms) << std::endl;
  std::cout << "sizeof(tExecutionContext)=" << sizeof(tGPUExecutionContext) << std::endl;
  std::cout << "sizeof(tWorkToken)=" << sizeof(tWorkToken) << std::endl;
  std::cout << "sizeof(cRGBColor)=" << sizeof(cRGBColor) << std::endl;
#endif

  Parms->Width = Width;
  Parms->Height = Height;
  Parms->nPixels = nPixels;
  Parms->WorkgroupSize = Configuration.WorkgroupSize;
  Parms->nWorkgroups = Configuration.nWorkgroups;
  Parms->nThreads = Configuration.WorkgroupSize * Configuration.nWorkgroups;

  // All the offsets are character offsets.
  char *Buf = (char *) Parms->Buffer;
  Parms->ThreadTempExpected = (uint32_t *) (Buf);
  Parms->WorkQueue = (tWorkToken *) (Buf + ThreadTempSize);
  Parms->Image = (uint32_t *) (Buf + ThreadTempSize + WorkQueueSize);
#ifdef __Debug__
  Parms->ThreadDebugInfo = (tThreadDebugInfo *) (Buf + ThreadTempSize + WorkQueueSize + ImageSize);
#endif

  // Find interior color
  cRGBColor InteriorColor;
  InteriorColor.GetPixelColor(InputImage, Configuration.StartingPoints[0u]);
  Parms->InteriorColor =InteriorColor.ToUInt();

  // Set the fill color with a flag that will never match an interior color.
  Parms->FillColor = Configuration.FillColor.ToUInt() | 0xff000000u;

  // Create spanners.
  Parms->LeftSpanner =
      {
          ID    : "Left",
          Perp1 : &Parms->UpSpanner,
          Perp2 : &Parms->DownSpanner,
          xInc  : -1,
          yInc  : 0,
          FillColor: 0xffFF0000,
      };

  Parms->RightSpanner =
      {
          ID    : "Right",
          Perp1 : &Parms->UpSpanner,
          Perp2 : &Parms->DownSpanner,
          xInc : 1,
          yInc : 0,
          FillColor: 0xff00FF00,
      };

  Parms->UpSpanner =
      {
          ID    : "Up",
          Perp1 : &Parms->LeftSpanner,
          Perp2 : &Parms->RightSpanner,
          xInc : 0,
          yInc : -1,
          FillColor: 0xff0000FF,
      };

  Parms->DownSpanner =
      {
          ID    : "Down",
          Perp1 : &Parms->LeftSpanner,
          Perp2 : &Parms->RightSpanner,
          xInc : 0,
          yInc : 1,
          FillColor: 0xff00FFFF,
      };

#ifdef __Debug__
  std::cout << "****************************** Initial Parms ******************************" << std::endl;
  std::cout << *Parms << std::endl;
  std::cout << "****************************** Initial Parms End******************************" << std::endl;
#endif

#ifdef __Debug__
  std::cout << "****************************** Begin Execution ******************************" << std::endl;
#endif

  // Create a kernel object
  cl_kernel PartitioningParaFill = OpenCLConfiguration.CreateKernel(KernelFunctionNames[0]);

#ifdef __Debug__
  std::cout << "PartitioningParaFill  CL_KERNEL_PRIVATE_MEM_SIZE: " << sOpenCLConfiguration::GetKernelWorkGroupInfo<cl_ulong>(PartitioningParaFill, OpenCLConfiguration, CL_KERNEL_PRIVATE_MEM_SIZE) << std::endl;
  std::cout << "PartitioningParaFill    CL_KERNEL_LOCAL_MEM_SIZE: " << sOpenCLConfiguration::GetKernelWorkGroupInfo<cl_ulong>(PartitioningParaFill, OpenCLConfiguration, CL_KERNEL_LOCAL_MEM_SIZE) << std::endl;
  std::cout << "PartitioningParaFill   CL_KERNEL_WORK_GROUP_SIZE: " << sOpenCLConfiguration::GetKernelWorkGroupInfo<size_t>(PartitioningParaFill, OpenCLConfiguration, CL_KERNEL_WORK_GROUP_SIZE) << std::endl;
#endif

  // Warm-up GPU.
  GPUParaFillCycle(OpenCLConfiguration, PartitioningParaFill, MasterImage, Parms, Configuration);

  double ExecutionTime = 0.0;
  for (auto i = 0u; i < Configuration.nIterations; ++i)
  {
    ExecutionTime += GPUParaFillCycle(OpenCLConfiguration, PartitioningParaFill, MasterImage, Parms, Configuration);
  }

  Configuration.StatsOut() << "Platform=" << Configuration.Platform << std::endl;
  Configuration.StatsOut() << "PlatformName=\"" << OpenCLConfiguration.GetPlatformInfoString(OpenCLConfiguration.PlatformID, CL_PLATFORM_NAME) << "\"" << std::endl;
  Configuration.StatsOut() << "Device=" << Configuration.Device << std::endl;
  Configuration.StatsOut() << "DeviceName=\"" <<  OpenCLConfiguration.GetDeviceInfoString(CL_DEVICE_NAME) << "\"" << std::endl;
  Configuration.StatsOut() << "nWG=" << Configuration.nWorkgroups << std::endl;
  Configuration.StatsOut() << "WGSize=" << Configuration.WorkgroupSize << std::endl;
  Configuration.StatsOut() << "nThreads=" << Parms->nThreads << std::endl;
  Configuration.StatsOut() << "GPUExecution=" << ExecutionTime / double(Configuration.nIterations) << std::endl;

  CImg<unsigned char> OutputImage;
  OutputImage = InputImage;
  auto PixelIndex = 0u;
  for (auto y = 0; y < Parms->Height; ++y)
  {
    for (auto x = 0; x < Parms->Width; ++x)
    {
      auto Pixel = Parms->Image[PixelIndex++];
      OutputImage(x, y, 0, 2) = Pixel & 0xffu;
      Pixel >>= 8u;
      OutputImage(x, y, 0, 1) = Pixel & 0xffu;
      Pixel >>= 8u;
      OutputImage(x, y, 0, 0) = Pixel & 0xffu;
    }
  }

  OutputImage.save(Configuration.GPUOutputFilename);

#ifdef __Debug__
  std::cout << "****************************** Final Parms ******************************" << std::endl;
  std::cout << *Parms << std::endl;
  std::cout << "****************************** Final Parms End******************************" << std::endl;
#endif

  // Clean up.
  clSVMFree(OpenCLConfiguration, Parms);
  Parms = nullptr;

  return;
}

double GPUParaFillCycle(sOpenCLConfiguration &OpenCLConfiguration, cl_kernel ParaFillKernel,
    const std::vector<uint32_t> &MasterImage, tGPUExecutionContext *Parms, const tConfiguration &Configuration)
{
  cl_int rc = CL_SUCCESS;

  // Initialize queue.
  Parms->WorkQueueFront = 0u;
  Parms->WorkQueueRear = 0u;
  Parms->WorkQueueTempRear = 0u;

#ifdef __Debug__
  Parms->nEnqueued = 0u;
  Parms->nDequeued = 0u;
#endif
  Parms->nQueued = 0u;

#ifdef __Debug__
  // For debugging.
  Parms->CyclesDone = 0u;
  Parms->nThreadsWorking = 0u;;

  auto nThreads = Parms->WorkgroupSize * Parms->nWorkgroups;
  for (auto i = 0u; i < nThreads; ++i)
  {
    Parms->ThreadDebugInfo[i].CycleCount =
        Parms->ThreadDebugInfo[i].StarvedCycleCount =
        Parms->ThreadDebugInfo[i].SpannerCount = 0u;
  }
#endif

  std::memcpy(Parms->Image, MasterImage.data(), Parms->nPixels * sizeof(uint));

  // Add starting points to work queue.
  for (const auto &StartingPoint : Configuration.StartingPoints)
  {
    // Place first spanner in queue.
    Parms->WorkQueue[Parms->WorkQueueRear].Spanner = &Parms->RightSpanner;
    Parms->WorkQueue[Parms->WorkQueueRear].x = StartingPoint.x;
    Parms->WorkQueue[Parms->WorkQueueRear].y = StartingPoint.y;
    ++Parms->WorkQueueRear;
    ++Parms->WorkQueueTempRear;
    ++Parms->nQueued;
#ifdef __Debug__
    ++Parms->nEnqueued;
#endif
    if (StartingPoint.x > 0)
    {
      auto LeftPixel = Parms->Image[Index(StartingPoint.x - 1, StartingPoint.y, Parms->Width)];
      if (LeftPixel == Parms->InteriorColor)
      {
        Parms->WorkQueue[Parms->WorkQueueRear].Spanner = &Parms->LeftSpanner;
        Parms->WorkQueue[Parms->WorkQueueRear].x = StartingPoint.x - 1;
        Parms->WorkQueue[Parms->WorkQueueRear].y = StartingPoint.y;
        ++Parms->WorkQueueRear;
        ++Parms->WorkQueueTempRear;
        ++Parms->nQueued;
#ifdef __Debug__
        ++Parms->nEnqueued;
#endif
      }
    }
  }

  // Set kernel args.
  rc = clSetKernelArgSVMPointer(ParaFillKernel, 0, Parms);
  OpenCLConfiguration.HandleOpenCLError(rc, "clSetKernelArgSVMPointer", __FILE__, __LINE__);

  // Setup for RankSort kernel launch
  size_t Offset[] = { 0 };
  size_t ParaFillGlobalRange[] = { size_t(Parms->nWorkgroups * Parms->WorkgroupSize) };
  size_t ParaFillLocalRange[] = { Parms->WorkgroupSize };

  // Lanuch kernel.
  cl_event ProfileEvent;
  rc = clEnqueueNDRangeKernel(OpenCLConfiguration, ParaFillKernel, 1, Offset, ParaFillGlobalRange, ParaFillLocalRange,
      0u, NULL, &ProfileEvent);
  OpenCLConfiguration.HandleOpenCLError(rc, "clEnqueueNDRangeKernel", __FILE__, __LINE__);

  clFlush(OpenCLConfiguration);

  cl_event WaitEvents[] = { ProfileEvent };
  clWaitForEvents(1, WaitEvents);

  clFinish(OpenCLConfiguration);

  //    CL_PROFILING_COMMAND_QUEUED   cl_ulong  A 64-bit value that describes the current device time counter in nanoseconds when the command identified by event is enqueued in a command-queue by the host.
  //    CL_PROFILING_COMMAND_SUBMIT   cl_ulong  A 64-bit value that describes the current device time counter in nanoseconds when the command identified by event that has been enqueued is submitted by the host to the device associated with the command-queue.
  //    CL_PROFILING_COMMAND_START  cl_ulong  A 64-bit value that describes the current device time counter in nanoseconds when the command identified by event starts execution on the device.
  //    CL_PROFILING_COMMAND_END  cl_ulong  A 64-bit value that describes the current device time counter in nanoseconds when the command identified by event has finished execution on the device.
  //    CL_PROFILING_COMMAND_COMPLETE   cl_ulong  A 64-bit value that describes the current device time counter in nanoseconds when the command identified by event and any child commands enqueued by this command on the device have finished execution.

  cl_ulong StartTime;
  cl_ulong EndTime;
  clGetEventProfilingInfo(ProfileEvent, CL_PROFILING_COMMAND_START, sizeof(StartTime), &StartTime, NULL);
  OpenCLConfiguration.HandleOpenCLError(rc, "clGetEventProfilingInfo", __FILE__, __LINE__);

  clGetEventProfilingInfo(ProfileEvent, CL_PROFILING_COMMAND_END, sizeof(EndTime), &EndTime, NULL);
  OpenCLConfiguration.HandleOpenCLError(rc, "clGetEventProfilingInfo", __FILE__, __LINE__);

  double ExecutionTime = double(EndTime - StartTime) / 1000000000.0;

  clReleaseEvent(ProfileEvent);

#ifdef __Debug__
  // For debugging and testing.
  CImg<unsigned char> OutputImage(Parms->Width, Parms->Height, 1, 3);
  auto PixelIndex = 0u;
  for (auto y = 0; y < Parms->Height; ++y)
  {
    for (auto x = 0; x < Parms->Width; ++x)
    {
      auto Pixel = Parms->Image[PixelIndex++];
      OutputImage(x, y, 0, 2) = Pixel & 0xffu;
      Pixel >>= 8u;
      OutputImage(x, y, 0, 1) = Pixel & 0xffu;
      Pixel >>= 8u;
      OutputImage(x, y, 0, 0) = Pixel & 0xffu;
    }
  }

  static auto ErrorCount = 0;
  if (OutputImage != CorrectImg)
  {
    std::cerr << "***************************Bad output image.***************************" << std::endl;
    OutputImage.save("./Error-.png", ++ErrorCount);
  }
#endif

  return ExecutionTime;
}
