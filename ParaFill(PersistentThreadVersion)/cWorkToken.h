#ifndef cWorkToken_h
#define cWorkToken_h

#include <ostream>
#include "cSpanner.h"

typedef struct sWorkToken
{
  tSpanner *Spanner;
  int x;
  int y;
} tWorkToken;

#define DumpField(Field) #Field "=" << WorkToken.Field
#define DumpHexField(Field) #Field "=" << Hex(WorkToken.Field)
#define DumpAddrField(Field) #Field "=" << Addr(WorkToken.Field)
inline std::ostream &operator << (std::ostream &Stream, const tWorkToken &WorkToken)
{
  if (WorkToken.Spanner)
  {
    Stream << DumpAddrField(Spanner) << "[" << *WorkToken.Spanner << "], " << DumpField(x) << ", " << DumpField(y);
  }
  else
  {
    Stream << DumpAddrField(Spanner);

  }
  return Stream;
}
#undef DumpField
#undef DumpHexField
#undef DumpAddrField

#endif
