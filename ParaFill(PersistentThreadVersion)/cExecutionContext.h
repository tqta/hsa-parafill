#ifndef cExecutionContext_h
#define cExecutionContext_h

#include <sstream>

// Utility functions

// (x,y) to index
inline unsigned int Index(unsigned int x, unsigned int y, unsigned int Width)
{
  return y * Width + x;
}

// Convert address to hex string.
inline std::string Addr(const void *Ad)
{
  std::ostringstream ss;
  ss << (void *) (((unsigned char *) Ad) - ((unsigned char *) nullptr));
  return ss.str();
}

// Convert size_t to hex string.
inline std::string Hex(size_t Val)
{
  return Addr((void *) Val);
}

// Align to next base.
inline size_t Align(size_t Val, size_t Base)
{
  return (Val % Base) ? Val + Base - (Val % Base) : Val;
}

// ********************************Spanner********************************
typedef struct sSpanner
{
  const char *ID;
  struct sSpanner *Perp1;
  struct sSpanner *Perp2;
  int xInc;
  int yInc;
  uint32_t FillColor;
} tSpanner;

#define DumpField(Field) #Field "=" << Spanner.Field
#define DumpHexField(Field) #Field "=" << Hex(Spanner.Field)
#define DumpAddrField(Field) #Field "=" << Addr(Spanner.Field)
inline std::ostream &operator << (std::ostream &Stream, const tSpanner &Spanner)
{
  Stream << DumpField(ID) << ", " << DumpAddrField(Perp1) << ", " << DumpAddrField(Perp2) << ", " << DumpField(xInc) << ", " << DumpField(yInc);
  return Stream;
}
#undef DumpField
#undef DumpHexField
#undef DumpAddrField

// ********************************WorkToken********************************
typedef struct sWorkToken
{
  tSpanner *Spanner;
  int x;
  int y;
} tWorkToken;

#define DumpField(Field) #Field "=" << WorkToken.Field
#define DumpHexField(Field) #Field "=" << Hex(WorkToken.Field)
#define DumpAddrField(Field) #Field "=" << Addr(WorkToken.Field)
inline std::ostream &operator << (std::ostream &Stream, const tWorkToken &WorkToken)
{
  if (WorkToken.Spanner)
  {
    Stream << DumpAddrField(Spanner) << "[" << *WorkToken.Spanner << "], " << DumpField(x) << ", " << DumpField(y);
  }
  else
  {
    Stream << DumpAddrField(Spanner);

  }
  return Stream;
}
#undef DumpField
#undef DumpHexField
#undef DumpAddrField

// ********************************ExecutionContext********************************
typedef struct sGPUExecutionContext
{
  uint32_t *OldQueueFront;
  uint32_t *OldQueueRear;
  uint32_t *Image;
  tWorkToken *WorkQueue;
  tSpanner LeftSpanner;
  tSpanner RightSpanner;
  tSpanner UpSpanner;
  tSpanner DownSpanner;
  int Width;
  int Height;
  int nPixels;
  uint32_t WorkgroupSize;
  uint32_t nWorkgroups;

  // Colors are stored 0xxxRRGGBB
  uint32_t InteriorColor;
  uint32_t FillColor;

  uint32_t QueueSize;
  uint32_t WorkQueueFront;
  uint32_t WorkQueueRear;
  uint32_t WorkQueueTempRear;
  // Debugging stuff goes here.
  uint32_t CyclesDone;
  tWorkToken ActiveWork[64u];
  double Buffer[0];
} tGPUExecutionContext;

#define DumpField(Field) #Field "=" << Context.Field
#define DumpHexField(Field) #Field "=" << Hex(Context.Field)
#define DumpAddrField(Field) #Field "=" << Addr(Context.Field)
#define DumpStructField(Field) #Field "(" << Addr(&Context.Field) << ") [" << Context.Field << "]"
inline std::ostream &operator << (std::ostream &Stream, const tGPUExecutionContext &Context)
{
  Stream << DumpField(Width) << std::endl;
  Stream << DumpField(Height) << std::endl;
  Stream << DumpField(nPixels) << std::endl;
  Stream << DumpHexField(InteriorColor) << std::endl;
  Stream << DumpHexField(FillColor) << std::endl;
  Stream << DumpField(WorkgroupSize) << std::endl;
  Stream << DumpField(nWorkgroups) << std::endl;
  Stream << DumpAddrField(OldQueueFront);
  for (auto i = 0u; i < Context.nWorkgroups; ++i)
  {
    Stream << ", " << "[" << i << "]=" << Context.OldQueueFront[0];
  }
  Stream << std::endl;
  Stream << DumpAddrField(OldQueueRear);
  for (auto i = 0u; i < Context.nWorkgroups; ++i)
  {
    Stream << ", " << "[" << i << "]=" << Context.OldQueueRear[0];;
  }
  Stream << std::endl;
  Stream << DumpStructField(LeftSpanner) << std::endl;
  Stream << DumpStructField(RightSpanner) << std::endl;
  Stream << DumpStructField(UpSpanner) << std::endl;
  Stream << DumpStructField(DownSpanner) << std::endl;
  Stream << DumpAddrField(Image) << std::endl;
  Stream << DumpField(WorkQueueFront) << std::endl;
  Stream << DumpField(WorkQueueRear) << std::endl;
  Stream << DumpField(WorkQueueTempRear) << std::endl;
  Stream << DumpAddrField(WorkQueue) << std::endl;
  for (auto i = Context.WorkQueueFront; i < Context.WorkQueueRear; ++i)
  {
    Stream << "  Q[" << i << "]: " << Context.WorkQueue[i] << std::endl;
  }
  Stream << "************************************Debug Info************************************" << std::endl;
  Stream << DumpField(CyclesDone) << std::endl;
  for (auto i = 0u; i < 64u; ++i)
  {
    if (Context.ActiveWork[i].Spanner)
    {
      // Only print work with a spanner.  (unstarved threads)
      Stream << "  ActiveWork[" << i << "]: " << Context.ActiveWork[i] << std::endl;
    }
  }

  return Stream;
}
#undef DumpField
#undef DumpHexField
#undef DumpAddrField
#undef DumpStructField

#endif
