#ifndef cPoint_h
#define cPoint_h

#include <ostream>
#include <string>

class cPoint
{
public:
  cPoint(void);
  cPoint(int vx, int vy);
  cPoint(const cPoint &vPoint);
  virtual ~cPoint(void);

public:
  virtual std::string ID(void) const;

public:
  inline cPoint &operator +=(const cPoint &rhs) {x += rhs.x; y += rhs.y; return *this;};
  inline cPoint &operator -=(const cPoint &rhs) {x -= rhs.x; y -= rhs.y; return *this;};
  inline cPoint operator +(const cPoint &rhs) {cPoint Temp; Temp.x = x + rhs.x; Temp.y = y + rhs.y; return Temp;};
  inline cPoint operator -(const cPoint &rhs) {cPoint Temp; Temp.x = x - rhs.x; Temp.y = y - rhs.y; return Temp;};

public:
  int x;
  int y;
};

// Some handy points.
extern cPoint PointUp;
extern cPoint PointDown;
extern cPoint PointRight;
extern cPoint PointLeft;

// Interface with I/O system
std::ostream &operator<<(std::ostream &Stream, const cPoint &Point);
#endif
