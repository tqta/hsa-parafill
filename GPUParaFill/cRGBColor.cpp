#include "cRGBColor.h"

#include <sstream>

cRGBColor::cRGBColor(void)
{
  return;
}

cRGBColor::cRGBColor(unsigned char vr, unsigned char vg, unsigned char vb) : r(vr), g(vg), b(vb), z(0u)
{
  return;
}

cRGBColor::cRGBColor(const cRGBColor &vRGBColor) : r(vRGBColor.r), g(vRGBColor.g), b(vRGBColor.b), z(vRGBColor.z)
{
  return;
}

cRGBColor::~cRGBColor(void)
{
  return;
}

std::string cRGBColor::ID(void) const
{
  std::ostringstream oss;
  oss << "(" << (unsigned int)(r) << ", " << (unsigned int)(g) << ", " << (unsigned int)(b) << ")";

  return oss.str();
}

void cRGBColor::GetPixelColor(const CImg<unsigned char> &Image, const cPoint &Location)
{
  r = Image(Location.x, Location.y, 0u);
  g = Image(Location.x, Location.y, 1u);
  b = Image(Location.x, Location.y, 2u);

  return;
}

void cRGBColor::GetPixelColor(const CImg<unsigned char> &Image, unsigned int x, unsigned int y)
{
  r = Image(x, y, 0u);
  g = Image(x, y, 1u);
  b = Image(x, y, 2u);

  return;
}

// Interface with I/O system
std::ostream &operator<<(std::ostream &Stream, const cRGBColor &RGBColor)
{
  Stream << RGBColor.ID();

  return Stream;
}
