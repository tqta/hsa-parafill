#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <CL/opencl.h>

#include "cPoint.h"
#include "cRGBColor.h"
#include "cStack.h"
#include "cSpanInfo.h"

#include "CImg.h"
using namespace cimg_library;
#ifndef cimg_display
#define cimg_display 0
#endif

#define INVALID -9999

//  Configuration:
// File names.
std::string InputFile("Jigsaw.bmp");
std::string OutputFile("JigsawOut.png");
std::string MaskFile("Mask.png");

// Initial points.
cPoint LittleArea(100u, 100u);
cPoint BigArea(100u, 300u);

// Starting point is the initial fill point, and defines the interior color;
//cPoint StartingPoint(LittleArea);
cPoint StartingPoint(BigArea);

// Colors
cRGBColor InteriorColor;
cRGBColor ConnectColor(0u, 0u, 255u);

// Images
CImg<unsigned char> InputImage;
CImg<unsigned char> MaskImage;

// check if a point is an internal point (CPU version)
inline bool IsInterior(const cPoint &Location) {return !MaskImage(Location.x, Location.y);};

// initialize original images
void Initialize(void);

inline int encodeRGB(int r, int g, int b) {return (r << 16) | (g << 8) | b;};

// OCL components
cl_context context;
cl_command_queue commandQueue;
cl_platform_id platform;
cl_uint deviceCount;
cl_device_id* devices;
cl_kernel kernel;
cl_program program;

size_t wgSize = 64;
size_t globalSize = wgSize * 8;

typedef struct Point_Struct {
  int x;  // x-coordinate
  int y;  // y-coordinate
} Point;

// each sNode has x, y coordinates
// spanning direction
// and pointer to the next sNode
typedef struct sNode_Struct {
  Point spanStart;
  Point spanDirection; // (0,1)-Up, (0,-1)-Down, (1,0)-Right, (-1,0)-Left
  unsigned long nextAddr;
} sNode;

// height & width of the image
int height, width;

// SVM OCL structures storing the images
int* inputImgCL;
int* maskImgCL;

// stack structures
sNode* stack;
int* stackSize;
int* stackCounter;
unsigned long* topAddr;
unsigned int* nextFreeSlot;
sNode* freeNodes;

unsigned int maxNumFreeNodes = 100000;

int* numWorkingThreads;

// initialize OCL structures
void initializeOCLVars(void);

// initialize inputImgCL & maskImgCL
void initializeOCLImgs(void);

// initialize stack structures
void initializeStack(void);

// set up kernel arguments
void setupKernelArgs(void);

// convert CL image --> original image
void revertCLImg(void);

void printDeviceInfo();

std::string getOCLErrCode(cl_int err);

void checkOCLError(cl_int err) {
  if (err != CL_SUCCESS){
    std::cout << getOCLErrCode(err) << std::endl;
    exit(-1);
  }
}

char* readKernelSource(const char *fileName);

void freeCLObjects();

// ------------------------------------------------------------------
int main(int argc, char *argv[])
{
  // Initialize image, mask, and interior color.
  Initialize();

  std::cout << "Image size = " << height * width;

  initializeOCLVars();

  initializeOCLImgs();

  initializeStack();

  setupKernelArgs();

  cl_int err = 0;

  // ***************** DEBUG **********************
  int* debug = (int*) clSVMAlloc(context,
                                  CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                                  sizeof(int)*64,
                                  0);

  err = clSetKernelArgSVMPointer(kernel, 12, (void*)debug);
  checkOCLError(err);

  // launch kernel
  std::cout << "Running kernel ... " << std::endl;
  err = clEnqueueNDRangeKernel(commandQueue, kernel, 1, NULL, &globalSize, &wgSize, 0, NULL, NULL);
  checkOCLError(err);

  err = clFinish(commandQueue);
  checkOCLError(err);
  std::cout << "Completed kernel ... " << std::endl;
  std::cout << "Stack size " << stackSize[0] << std::endl;
  std::cout << "numWorkingThreads size " << numWorkingThreads[0] << std::endl;
  // ***************** DEBUG **********************
//  for (int i = 0; i < nextFreeSlot[0]; i++)
//    std::cout << freeNodes[i].spanStart.x << " " << freeNodes[i].spanStart.y << std::endl;

  // convert inputImgCL -> InputImage
  revertCLImg();

  // Save result with connected region colored.
  InputImage.save(OutputFile.c_str());

  // free memory
  freeCLObjects();

  // And that's it.
  return 0;
}

void revertCLImg(void){
  for (int row = 0; row < height; row++)
    for (int col = 0; col < width; col++){
      int rgb = inputImgCL[row*width+col];

      InputImage(row, col, 0, 0) = (rgb >> 16) & 0xFF;
      InputImage(row, col, 0, 1) = (rgb >> 8) & 0xFF;
      InputImage(row, col, 0, 2) = rgb & 0xFF;
    }
}

void Initialize(void)
{
  // Read in image.
  InputImage.load(InputFile.c_str());

  // Find interior color
  InteriorColor.GetPixelColor(InputImage, StartingPoint);

  std::cout << "Interior color: " << InteriorColor << std::endl;

  // Create monochrome mask using image and interior color.
  CImg<unsigned char> TempMask(InputImage.width(), InputImage.height(), 1u, 1u);
  MaskImage = TempMask;
  cRGBColor PixelColor;
  for (unsigned int x = 0u; x < (unsigned int) (InputImage.width()); ++x)
  {
    for (unsigned int y = 0u; y < (unsigned int) (InputImage.height()); ++y)
    {
      PixelColor.GetPixelColor(InputImage, x, y);
      // Interior colors are black, all others are white.
      MaskImage(x, y) = (PixelColor == InteriorColor) ? 0u : 255u;
    }
  }

  // Save the mask.
  MaskImage.save(MaskFile.c_str());

  height = InputImage.height();
  width = InputImage.width();

  return;
}

void setupKernelArgs(){
  cl_int err = 0;
  err = clSetKernelArgSVMPointer(kernel, 0, (void*)inputImgCL);
  checkOCLError(err);

  err = clSetKernelArgSVMPointer(kernel, 1, (void*)maskImgCL);
  checkOCLError(err);

  err = clSetKernelArg(kernel, 2, sizeof(cl_int), (void*)&width);
  checkOCLError(err);

  err = clSetKernelArg(kernel, 3, sizeof(cl_int), (void*)&height);
  checkOCLError(err);

  err = clSetKernelArgSVMPointer(kernel, 4, (void*)stack);
  checkOCLError(err);

  err = clSetKernelArgSVMPointer(kernel, 5, (void*)topAddr);
  checkOCLError(err);

  err = clSetKernelArgSVMPointer(kernel, 6, (void*)stackSize);
  checkOCLError(err);

  err = clSetKernelArgSVMPointer(kernel, 7, (void*)stackCounter);
  checkOCLError(err);

  err = clSetKernelArgSVMPointer(kernel, 8, (void*)numWorkingThreads);
  checkOCLError(err);

  err = clSetKernelArgSVMPointer(kernel, 9, (void*)freeNodes);
  checkOCLError(err);

  err = clSetKernelArgSVMPointer(kernel, 10, (void*)nextFreeSlot);
  checkOCLError(err);

  err = clSetKernelArg(kernel, 11, sizeof(cl_int), (void*)&maxNumFreeNodes);
  checkOCLError(err);
}

void initializeStack(void){
  // initialize the stack with the starting pixel
  stack = (sNode*) clSVMAlloc(context,
                              CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                              sizeof(sNode),
                              0);
  stack[0].spanStart = {StartingPoint.x, StartingPoint.y};
  stack[0].spanDirection = {1,0};
  stack[0].nextAddr = 0u;

  // initially stackSize is 1
  stackSize = (int*) clSVMAlloc(context,
                                CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                                sizeof(int),
                                0);
  stackSize[0] = 1;

  // initially stackCounter is 1
  stackCounter = (int*) clSVMAlloc(context,
                                   CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                                   sizeof(int),
                                   0);
  stackCounter[0] = 1;

  // initially topAddr is the address of the only node in the stack
  topAddr = (unsigned long*) clSVMAlloc(context,
                                        CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                                        sizeof(unsigned long),
                                        0);
  topAddr[0] = (unsigned long) stack;

  // initially nextFreeSlot is 0
  nextFreeSlot = (unsigned int*) clSVMAlloc(context,
                                            CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                                            sizeof(unsigned int),
                                            0);
  nextFreeSlot[0] = 0;

  // initially all freeNodes are available to use
  // spanStart = {INVALID,INVALID}
  freeNodes = (sNode*) clSVMAlloc(context,
                                  CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                                  maxNumFreeNodes*sizeof(sNode),
                                  0);
  for (int i = 0; i < maxNumFreeNodes; i++)
    freeNodes[i].spanStart = {INVALID,INVALID};

  // initially, 0 thread is working
  numWorkingThreads = (int*) clSVMAlloc(context,
                                        CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                                        sizeof(int),
                                        0);
  numWorkingThreads[0] = 0;
}

void initializeOCLImgs(void){
  int imgSize = height * width;

  inputImgCL = (int*) clSVMAlloc(context,
                                 CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                                 sizeof(int) * imgSize,
                                 0);

  maskImgCL = (int*) clSVMAlloc (context,
                                 CL_MEM_SVM_FINE_GRAIN_BUFFER|CL_MEM_SVM_ATOMICS,
                                 sizeof(int) * imgSize,
                                 0);

  for (int row = 0; row < height; row++)
    for (int col = 0; col < width; col++){
      inputImgCL[row*width+col] = encodeRGB(InputImage(row, col, 0, 0),
                                            InputImage(row, col, 0, 1),
                                            InputImage(row, col, 0, 2));
      maskImgCL[row*width+col] = MaskImage(row,col,0,0);
    }
}

void initializeOCLVars(void){
  cl_int err = 0;

  // Get platform
  err = clGetPlatformIDs(1, &platform, NULL);
  checkOCLError(err);

  // Get the AMD devices
  err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &deviceCount);
  std::cout << "Number of devices: " << deviceCount << std::endl;

  devices = (cl_device_id*) malloc (deviceCount*sizeof(cl_device_id));

  err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, deviceCount, devices, NULL);
  checkOCLError(err);

  printDeviceInfo();

  int deviceID = 0; // by default, choose the first device

  // Create the context
  context = clCreateContext(0, 1, devices+deviceID, NULL, NULL, &err);
  checkOCLError(err);

  // create host command queue
  commandQueue = clCreateCommandQueue(context, devices[deviceID], NULL, &err);
  checkOCLError(err);

  // Read the OpenCL kernel from source file
  char* source = readKernelSource("Kernel.cl");

  // Create a program with kernel source file
  program = clCreateProgramWithSource(context, 1, (const char **)&source, NULL, &err);
  checkOCLError(err);

  // Build the program
  std::cout << "Compiling with opencl 2.0 flag" << std::endl;
  err = clBuildProgram(program, 1, devices+deviceID, "-save-temps -cl-std=CL2.0", NULL, NULL);

  if (err != CL_SUCCESS){
    std::cout << "Error in clBuildProgram: " << getOCLErrCode(err) << std::endl;
    size_t build_log_size;
    err = clGetProgramBuildInfo(program, devices[deviceID], CL_PROGRAM_BUILD_LOG, 0, NULL, &build_log_size);
    char log[build_log_size];
    err = clGetProgramBuildInfo(program, devices[deviceID], CL_PROGRAM_BUILD_LOG, build_log_size, log, NULL);
    std::cout << log << std::endl;
    exit(-1);
  }

  // Create kernel
  kernel = clCreateKernel(program, "ParaFillKernel", &err);
  checkOCLError(err);
}

void freeCLObjects(){
  // release OpenCL objects
  if(commandQueue)clReleaseCommandQueue(commandQueue);
  if(context)clReleaseContext(context);
  if(program)clReleaseProgram(program);
  if(kernel)clReleaseKernel(kernel);

  // free mem pointers
  free(devices);

  clSVMFree(context, inputImgCL);
  clSVMFree(context, maskImgCL);
  clSVMFree(context, stack);
  clSVMFree(context, stackSize);
  clSVMFree(context, stackCounter);
  clSVMFree(context, topAddr);
  clSVMFree(context, nextFreeSlot);
  clSVMFree(context, freeNodes);
  clSVMFree(context, numWorkingThreads);
}

char* readKernelSource(const char *fileName){
  /*
   * Load the kernel source code into *char source
   */
  FILE *fp;
  char *source;
  size_t size;

  fp = fopen(fileName, "r");
  if (!fp) {
    fprintf(stderr, "Failed to load kernel.\n");
    exit(1);
  }

  // Measure the size of source file
  fseek(fp, 0, SEEK_END);
  size = ftell(fp);

  fseek(fp, 0, SEEK_SET);

  source = (char*)malloc(size+1);
  size = fread(source, 1, size, fp);
  source[size]='\0';
  fclose( fp );

  return source;
}

void printDeviceInfo(){
  cl_int err = 0;
  // Print device info
  for (unsigned int i = 0; i < deviceCount; i++){
    printf("*** Device # %d ***\n", i);
    char buffer[10240];
    cl_uint buf_uint;
    cl_ulong buf_ulong;
    err = clGetDeviceInfo(devices[i], CL_DEVICE_NAME, sizeof(buffer), buffer, NULL);
    printf("  DEVICE_NAME = %s\n", buffer);
    err = clGetDeviceInfo(devices[i], CL_DEVICE_VENDOR, sizeof(buffer), buffer, NULL);
    printf("  DEVICE_VENDOR = %s\n", buffer);
    err = clGetDeviceInfo(devices[i], CL_DEVICE_VERSION, sizeof(buffer), buffer, NULL);
    printf("  DEVICE_VERSION = %s\n", buffer);
    err = clGetDeviceInfo(devices[i], CL_DRIVER_VERSION, sizeof(buffer), buffer, NULL);
    printf("  DRIVER_VERSION = %s\n", buffer);
    err = clGetDeviceInfo(devices[i], CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(buf_uint), &buf_uint, NULL);
    printf("  DEVICE_MAX_COMPUTE_UNITS = %u\n", (unsigned int)buf_uint);
    err = clGetDeviceInfo(devices[i], CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(buf_uint), &buf_uint, NULL);
    printf("  DEVICE_MAX_CLOCK_FREQUENCY = %u\n", (unsigned int)buf_uint);
    err = clGetDeviceInfo(devices[i], CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(buf_ulong), &buf_ulong, NULL);
    printf("  DEVICE_GLOBAL_MEM_SIZE = %llu\n", (unsigned long long)buf_ulong);
    err = clGetDeviceInfo(devices[i], CL_DEVICE_LOCAL_MEM_SIZE, sizeof(buf_ulong), &buf_ulong, NULL);
    printf("  DEVICE_LOCAL_MEM_SIZE = %llu\n", (unsigned long long)buf_ulong);
    err = clGetDeviceInfo(devices[i], CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, sizeof(buf_ulong), &buf_ulong, NULL);
    printf("  DEVICE_GLOBAL_MEM_CACHE_SIZE = %llu\n", (unsigned long long)buf_ulong);
  }
}

std::string getOCLErrCode(cl_int err){
  switch (err) {
    case CL_SUCCESS:                            return "Success!";
    case CL_DEVICE_NOT_FOUND:                   return "Device not found.";
    case CL_DEVICE_NOT_AVAILABLE:               return "Device not available";
    case CL_COMPILER_NOT_AVAILABLE:             return "Compiler not available";
    case CL_MEM_OBJECT_ALLOCATION_FAILURE:      return "Memory object allocation failure";
    case CL_OUT_OF_RESOURCES:                   return "Out of resources";
    case CL_OUT_OF_HOST_MEMORY:                 return "Out of host memory";
    case CL_PROFILING_INFO_NOT_AVAILABLE:       return "Profiling information not available";
    case CL_MEM_COPY_OVERLAP:                   return "Memory copy overlap";
    case CL_IMAGE_FORMAT_MISMATCH:              return "Image format mismatch";
    case CL_IMAGE_FORMAT_NOT_SUPPORTED:         return "Image format not supported";
    case CL_BUILD_PROGRAM_FAILURE:              return "Program build failure";
    case CL_MAP_FAILURE:                        return "Map failure";
    case CL_INVALID_VALUE:                      return "Invalid value";
    case CL_INVALID_DEVICE_TYPE:                return "Invalid device type";
    case CL_INVALID_PLATFORM:                   return "Invalid platform";
    case CL_INVALID_DEVICE:                     return "Invalid device";
    case CL_INVALID_CONTEXT:                    return "Invalid context";
    case CL_INVALID_QUEUE_PROPERTIES:           return "Invalid queue properties";
    case CL_INVALID_COMMAND_QUEUE:              return "Invalid command queue";
    case CL_INVALID_HOST_PTR:                   return "Invalid host pointer";
    case CL_INVALID_MEM_OBJECT:                 return "Invalid memory object";
    case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:    return "Invalid image format descriptor";
    case CL_INVALID_IMAGE_SIZE:                 return "Invalid image size";
    case CL_INVALID_SAMPLER:                    return "Invalid sampler";
    case CL_INVALID_BINARY:                     return "Invalid binary";
    case CL_INVALID_BUILD_OPTIONS:              return "Invalid build options";
    case CL_INVALID_PROGRAM:                    return "Invalid program";
    case CL_INVALID_PROGRAM_EXECUTABLE:         return "Invalid program executable";
    case CL_INVALID_KERNEL_NAME:                return "Invalid kernel name";
    case CL_INVALID_KERNEL_DEFINITION:          return "Invalid kernel definition";
    case CL_INVALID_KERNEL:                     return "Invalid kernel";
    case CL_INVALID_ARG_INDEX:                  return "Invalid argument index";
    case CL_INVALID_ARG_VALUE:                  return "Invalid argument value";
    case CL_INVALID_ARG_SIZE:                   return "Invalid argument size";
    case CL_INVALID_KERNEL_ARGS:                return "Invalid kernel arguments";
    case CL_INVALID_WORK_DIMENSION:             return "Invalid work dimension";
    case CL_INVALID_WORK_GROUP_SIZE:            return "Invalid work group size";
    case CL_INVALID_WORK_ITEM_SIZE:             return "Invalid work item size";
    case CL_INVALID_GLOBAL_OFFSET:              return "Invalid global offset";
    case CL_INVALID_EVENT_WAIT_LIST:            return "Invalid event wait list";
    case CL_INVALID_EVENT:                      return "Invalid event";
    case CL_INVALID_OPERATION:                  return "Invalid operation";
    case CL_INVALID_GL_OBJECT:                  return "Invalid OpenGL object";
    case CL_INVALID_BUFFER_SIZE:                return "Invalid buffer size";
    case CL_INVALID_MIP_LEVEL:                  return "Invalid mip-map level";
    default: return "Unknown";
  }
}

