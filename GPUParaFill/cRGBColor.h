#ifndef CRGBCOLOR_H_
#define CRGBCOLOR_H_

#include <ostream>
#include <string>

#include "CImg.h"
using namespace cimg_library;
#include "cPoint.h"


class cRGBColor
{
public:
  cRGBColor(void);
  cRGBColor(unsigned char vr, unsigned char vg, unsigned char vb);
  cRGBColor(const cRGBColor &vRGBColor);
  virtual ~cRGBColor();

public:
  virtual std::string ID(void) const;

public:
  void GetPixelColor(const CImg<unsigned char> &Image, const cPoint &Location);
  void GetPixelColor(const CImg<unsigned char> &Image, unsigned int x, unsigned int y);

public:
  inline bool operator ==(const cRGBColor &rhs) const {return (r == rhs.r) && (b == rhs.b) && (g == rhs.g);};

public:
  unsigned char r;
  unsigned char g;
  unsigned char b;
  unsigned char z;
};

// Interface with I/O system
std::ostream &operator<<(std::ostream &Stream, const cRGBColor &RGBColor);
#endif
