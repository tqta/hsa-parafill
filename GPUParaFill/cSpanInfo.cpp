#include "cSpanInfo.h"

#include <sstream>

cSpanInfo::cSpanInfo(void) : SpanStart(cPoint(0u, 0u)), SpanDirection(cPoint(0u, 0u))
{
  return;
}

cSpanInfo::cSpanInfo(cPoint vSpanStart, cPoint vSpanDirection) : SpanStart(vSpanStart), SpanDirection(vSpanDirection)
{
  return;
}

cSpanInfo::cSpanInfo(const cSpanInfo &vSpanInfo) : SpanStart(vSpanInfo.SpanStart), SpanDirection(vSpanInfo.SpanDirection)
{
  return;
}

cSpanInfo::~cSpanInfo(void)
{
  return;
}

std::string cSpanInfo::ID(void) const
{
  std::ostringstream oss;
  oss << "[" << SpanStart << ", " << SpanDirection << "]";

  return oss.str();
}

// Interface with I/O system
std::ostream &operator<<(std::ostream &Stream, const cSpanInfo &SpanInfo)
{
  Stream << SpanInfo.ID();

  return Stream;
}
