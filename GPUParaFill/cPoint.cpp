#include "cPoint.h"

#include <sstream>

cPoint PointUp(0, 1);
cPoint PointDown(0, -1);
cPoint PointRight(1, 0);
cPoint PointLeft(-1, 0);

cPoint::cPoint(void) : x(0), y(0)
{
  return;
}

cPoint::cPoint(int vx, int vy) : x(vx), y(vy)
{
  return;
}

cPoint::cPoint(const cPoint &vPoint) : x(vPoint.x), y(vPoint.y)
{
  return;
}

cPoint::~cPoint(void)
{
  return;
}

std::string cPoint::ID(void) const
{
  std::ostringstream oss;
  oss << "(" << x << ", " << y << ")";

  return oss.str();
}

// Interface with I/O system
std::ostream &operator<<(std::ostream &Stream, const cPoint &Point)
{
  Stream << Point.ID();

  return Stream;
}
