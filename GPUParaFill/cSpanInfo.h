#ifndef cSpanInfo_h
#define cSpanInfo_h

#include <ostream>
#include <string>

#include "cPoint.h"

class cSpanInfo
{
public:
  cSpanInfo(void);
  cSpanInfo(cPoint vSpanStart, cPoint vSpanDirection);
  cSpanInfo(const cSpanInfo &vSpanInfo);
  virtual ~cSpanInfo(void);

public:
  virtual std::string ID(void) const;

public:
  cPoint SpanStart;
  cPoint SpanDirection;
};

// Interface with I/O system
std::ostream &operator<<(std::ostream &Stream, const cSpanInfo &SpanInfo);
#endif
