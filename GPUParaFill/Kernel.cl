#define TRUE 1
#define FALSE 0
#define INVALID -9999

typedef struct Point_Struct {
	int x;	// x-coordinate		
	int y;	// y-coordinate
} Point;

// each sNode has x, y coordinates
// spanning direction 
// and pointer to the next sNode
typedef struct sNode_Struct {
	Point spanStart;
	Point spanDirection; // (0,1)-Up, (0,-1)-Down, (1,0)-Right, (-1,0)-Left
	unsigned long nextAddr;
} sNode;

inline int encodeRGB(int r, int g, int b) {return (r << 16) | (g << 8) | b;};

int isInterior(int* maskImg, Point point, int width) {
	return (!maskImg[point.x * width + point.y]) ? TRUE : FALSE;
}

void push(sNode* stack,
		      atomic_int* stackSize,
          atomic_int* stackCounter,
		      atomic_ulong* topAddr,
		      sNode* newNode, 
		      Point point,
		      Point direction){
	newNode->spanStart = point;
	newNode->spanDirection = direction;

	// pull the current top address
	unsigned long myTopAddr = atomic_load_explicit(topAddr, 
												                         memory_order_seq_cst,
												                         memory_scope_device);
	
	// set new node to point to the old top
	newNode->nextAddr = myTopAddr;
		
	while(!atomic_compare_exchange_strong_explicit(topAddr, 
											                           &(newNode->nextAddr), 
										                     	       (unsigned long) newNode, 
									                     	         memory_order_seq_cst, 
										                     	       memory_order_relaxed)){
   		// loop until topAddr == &(newNode->nextAddr)
   		// newNode->nextAddr is automatically updated with the current
   		// topAddr value --> no need to reload the topAddr
  }
	
	// update the stack size
	atomic_fetch_add_explicit(stackSize, 
							              1, 
							              memory_order_seq_cst, 
							              memory_scope_device);

  // after that, update the stack counter
	atomic_fetch_add_explicit(stackCounter, 
							              1, 
							              memory_order_seq_cst, 
							              memory_scope_device);
}

sNode* pop(sNode* stack,
	         atomic_int* stackSize,
           atomic_int* stackCounter,
	         atomic_ulong* topAddr,
	         atomic_int* numWorkingThreads){
	int ret = 0;
	unsigned long myTopAddr, newTopAddr;
	
  if ((atomic_fetch_add_explicit(stackCounter, 
                                 -1, 
                                 memory_order_seq_cst, 
                                 memory_scope_device) - 1) < 0) {
    // this thread could not get work. Recover and let user know there is no work
    atomic_fetch_add_explicit(stackCounter, 
                              1, 
                              memory_order_seq_cst, 
                              memory_scope_device);

    return NULL;    
  }

  // decrement the stackSize b/c it's guaranteed this thread will get task
  atomic_fetch_add_explicit(stackSize, 
                             -1, 
                             memory_order_seq_cst, 
                             memory_scope_device);

	// pull the current top address
	myTopAddr = atomic_load_explicit(topAddr, 
									                 memory_order_seq_cst,
									                 memory_scope_device);
	
	// new top will be the node right below the current top
	// try to update the topAddr with the new top
	while (myTopAddr && !atomic_compare_exchange_strong_explicit(topAddr, 
											                                          &myTopAddr, 
											                                          ((sNode*)myTopAddr)->nextAddr, 
											                                          memory_order_seq_cst, 
											                                          memory_order_relaxed)) {

  }

	// give the caller the old stack top		  
	return (sNode*)myTopAddr;
}
		  
int addSpanner( int* maskImg, Point point, Point direction, 
                int width, int height,
                sNode* stack, atomic_int* stackSize, atomic_int* stackCounter, atomic_ulong* topAddr,
                sNode* freeNodes, atomic_uint* nextFreeSlot, unsigned int maxNumFreeNodes){
	if (isInterior(maskImg, point, width) && 
		    (point.x < width) && (point.x >= 0) &&
		    (point.y < height) && (point.y >= 0)){
		
		// get a free slot
		int mySlot;

    do {
      mySlot = atomic_fetch_add_explicit(nextFreeSlot, 1, memory_order_relaxed, memory_scope_device);

      if (mySlot >= maxNumFreeNodes) {
        // reset
        atomic_fetch_and(nextFreeSlot, 0x0000000);
      }
		} while (mySlot >= maxNumFreeNodes || freeNodes[mySlot].spanStart.x != INVALID);

		push(stack, stackSize, stackCounter, topAddr, freeNodes+mySlot, point, direction);
		
		return TRUE;
	}
	
	return FALSE;
}

__kernel void ParaFillKernel(
              __global int* inputImg,
              __global int* maskImg,
              const int width,
              const int height,

              __global sNode* stack,
              __global atomic_ulong* topAddr,		      // address of top node
              __global atomic_int* stackSize, 		    // current size of the stack
              __global atomic_int* stackCounter,      // second counter to avoid 

              __global atomic_int* numWorkingThreads, // current number of working threads

              __global sNode* freeNodes,	
              __global atomic_uint* nextFreeSlot,	    // index of the next free sNode 
              const unsigned int maxNumFreeNodes,

              __global int* debug
){
	// stackSize should be 1 initially
	// either there is at least one working thread (possibly spawn more work)
	// or stack still has at least one task
	while (atomic_load_explicit(numWorkingThreads, memory_order_relaxed, memory_scope_device) > 0 ||
		          atomic_load_explicit(stackSize, memory_order_relaxed, memory_scope_device) > 0){

		sNode* work = NULL;
		
		while ((work = pop(stack, stackSize, stackCounter, topAddr, numWorkingThreads)) != NULL){
			// update the number of working threads
			atomic_fetch_add_explicit(numWorkingThreads, 
								                1, 
								                memory_order_seq_cst, 
								                memory_scope_device);
									  
			// extract work info
			Point currPoint = work->spanStart;
			Point spanDirection = work->spanDirection;									  

      // mark this node to be INVALID (it can be reused later)
      (work->spanStart).x = INVALID;

			// figure out perpendicular directions
			Point perpDir1;
			Point perpDir2;
			
			if (spanDirection.x) {
				// UP
				perpDir1.x = 0;
				perpDir1.y = 1;
				
				// DOWN
				perpDir2.x = 0;
				perpDir2.y = -1; 
			} else {
				// Right
				perpDir1.x = 1;
				perpDir1.y = 0;
				
				// Left
				perpDir2.x = -1;
				perpDir2.y = 0; 
			}
						
			while (isInterior(maskImg, currPoint, width) &&
					    (currPoint.x < width) && (currPoint.x >= 0) &&
					    (currPoint.y < height) && (currPoint.y >= 0)) {
				// we have an interior point. the span continues
				// place connected color in image
				inputImg[currPoint.x*width+currPoint.y] = encodeRGB(0,0,255);
				
				// mark as exterior on mask
				maskImg[currPoint.x*width+currPoint.y] = 255;
				
				Point newPoint1 = {currPoint.x + perpDir1.x, currPoint.y + perpDir1.y};
				Point newPoint2 = {currPoint.x + perpDir2.x, currPoint.y + perpDir2.y};
				
				addSpanner(maskImg, newPoint1, perpDir1, 
                    width, height, 
                    stack, stackSize, stackCounter, 
                    topAddr, freeNodes, nextFreeSlot, maxNumFreeNodes);
				addSpanner(maskImg, newPoint2, perpDir2, 
                    width, height, 
                    stack, stackSize, stackCounter, 
                    topAddr, freeNodes, nextFreeSlot, maxNumFreeNodes);
				
				currPoint.x += spanDirection.x;
				currPoint.y += spanDirection.y;
			}
			
			// update the number of working threads
			atomic_fetch_add_explicit(numWorkingThreads, 
									  -1, 
									  memory_order_seq_cst, 
									  memory_scope_device);
		}	
  }
	return;
}
