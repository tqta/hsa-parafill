#ifndef cStack_h
#define cStack_h

#include <atomic>
#include <memory>

template<class StackType>
// My attempt at a lock free stack.
class cStack
{
private:
  struct sNode
  {
    StackType Data;
    sNode* Next;
    sNode(const StackType &vData) : Data(vData), Next(nullptr) {return;};
  };

public:
  cStack(void);
  ~cStack(void);

public:
  void Push(const StackType &Data);
  bool Pop(StackType &PoppedData);
  inline int GetStackSize(void) const {return StackSize.load(std::memory_order_acq_rel);};
  inline bool HasWork(void) const {return Head.load(std::memory_order_acq_rel);};

private:
  std::atomic<sNode *> Head;
  std::atomic<unsigned int> StackSize;
};


template<class StackType>
cStack<StackType>::cStack(void) : Head(nullptr)
{
  return;
}

template<class StackType>
cStack<StackType>::~cStack(void)
{
  return;
}

template<class StackType>
void cStack<StackType>::Push(const StackType &Data)
{
  // Allocate new stack node and save stacked data.
  sNode* NewNode = new sNode(Data);

  // New node's next points to the old top of stack.  Only requires atomicity, no memory ordering.
  NewNode->Next = Head.load(std::memory_order_relaxed);

  // If Head still pointing to NewNode->Next, exchange NewNode with Head.  Loop until exchange successful.
  while(!Head.compare_exchange_weak(NewNode->Next, NewNode, std::memory_order_release, std::memory_order_relaxed))
    ;

  // StackSize has increased.
  ++StackSize;

  return;
}

template<class StackType>
bool cStack<StackType>::Pop(StackType &PoppedData)
{
  // Remember head node.
  sNode *OldHead = Head.load(std::memory_order_relaxed);

  // If OldHead is still Head, swap with next.  On success, no memory reorder above or below permitted.
  bool PopSucceeded = OldHead && Head.compare_exchange_weak(OldHead, OldHead->Next, std::memory_order_acq_rel, std::memory_order_relaxed);

  if (PopSucceeded)
  {
    // Stack size has decreased.
    --StackSize;

    // Give caller the data at the top of stack.
    PoppedData = OldHead->Data;

    // Done with old stack node.
    delete OldHead;

    return true;
  }

  return false;
}

#endif
